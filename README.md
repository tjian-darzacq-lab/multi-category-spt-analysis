# Multi-category SPT analysis



## Description

This project contains code to analyze fast SPT data. It requires pre-processing using the CellPicker tool, which allows the user to manually select cells for downstream analysis based on their intensities in different channels, and to classify cells into multiple categories/cell types.

These scripts perform multiple common analyses typically used to examine SPT data, such as diffusivity estimation (using the SASPT package). Additionally, they examine correlations between diffusivity, morpholical features, protein expression levels, and cell cycle stages at a single-cell level.


## Authors and acknowledgment
Code by Thomas Graham, Sathvik Anantakrishnan, and Vinson Fan.
