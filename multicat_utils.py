"""
SADness_utils_new_MultiExp.py -- utilities for analysis of data output by CellPicker using SASPT's state array dataset
Compatible with multiple experiments and cell types. If cell type/category-specific analysis is desired, cells must be
sorted into categories using the CellPicker MATLAB GUI. Molecule tracking data is assumed to be in the form of CSV
files formatted according to the output of the quot Python package.

Functions by Thomas Graham and Sathvik Anantakrishnan
"""

import os
import re
import toml 
import pandas as pd
import numpy as np
from glob import glob
import scipy.io
from scipy import stats
import traceback
from saspt import StateArray, StateArrayDataset, RBME, load_detections
from matplotlib import pyplot as plt
import matplotlib.colors as mcolors
from PIL import Image
import pickle
import skimage
import seaborn as sns
import scipy
import sklearn.cluster
import math
import random
from matplotlib.ticker import ScalarFormatter, FormatStrFormatter
from skimage.morphology import binary_dilation
from skimage.restoration import rolling_ball
import matplotlib
# matplotlib.rcParams['pdf.fonttype'] = 42
# matplotlib.rcParams['ps.fonttype'] = 42

def make_roi_measurements_folder(maskmat, measfolder, snapsfolder, fullmaskvarname = 'masks'):
    '''
    Create a folder containing a CSV file for each ROI that describes the features of each cell in the ROI.

    Inputs:
        maskmat: path to output .mat file from CellPicker containing the masks and category IDs for each cell
        measfolder: path to the folder where the CSV files will be saved
        snapsfolder: path to the folder containing the images used to create the masks
        fullmaskvarname: name of the variable in the .mat file that contains the masks
    '''
    os.makedirs(measfolder,exist_ok = True)
    masks = scipy.io.loadmat(maskmat)
    nrange = masks['range'][0]     # range of file numbers (upper and lower bounds inclusive)

    for j in range(nrange[0],nrange[1]+1):
        currmask = masks[fullmaskvarname][0][j-1]

        # Added by TG 20230629 to deal with gaps in the file numbers
        if not os.path.exists(f"{snapsfolder}/{j}.tif"):
            continue
        
        im = np.array(Image.open(f"{snapsfolder}/{j}.tif"))
        
        rp = skimage.measure.regionprops(currmask, im)
        
        roi_bg_intensity = np.sum(im[currmask == 0])/np.sum(currmask == 0)
        roi_data = []
        for c in range(len(rp)):
            
            cell_data = [rp[c].centroid[0], rp[c].centroid[1], rp[c].orientation, rp[c].axis_major_length, rp[c].axis_minor_length,
                        rp[c].area, rp[c].area_filled, rp[c].eccentricity, rp[c].intensity_mean, roi_bg_intensity]
            if c == 0:
                roi_data = [cell_data]
            else:
                roi_data = np.append(roi_data, [cell_data], axis = 0)
        col_names = ['centroid-0', 'centroid-1', 'orientation', 'axis_major_length', 'axis_minor_length',
                     'area', 'area_filled', 'eccentricity', 'intensity_mean', 'background']
        index_vals = np.arange(len(rp))+1
        roi_df = pd.DataFrame(data = roi_data, index = index_vals, columns = col_names)
        roi_df.to_csv(measfolder+'/'+str(j)+'.csv', index = True)

def generate_SAD(sorted_csv_folders, experiments, alt_snapsfolders_names, alt_snapsfolders_list, min_trajs_per_cell=1, cat_names=None, cat_colors=None, outf='Figures', settings=None, subsampling=False):
    '''
    Generates a dictionary of SASPT state array datasets for each cell type (category).

    Inputs:
        sorted_csv_folders: list of paths (for each experiment) to cell-specific tracking data organized into sub-directories for each cell category
        experiments: list of paths to the parent folders of each experiment
        alt_snapsfolders_names: list of names of alternative channels to be analyzed
        alt_snapsfolders_list: list of paths (for each experiment) to directories containing images from alternative channels
        min_trajs_per_cell: minimum number of non-singlet trajectories required for a cell to be included in the analysis
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        settings: dictionary of settings for SASPT state array dataset generation
        subsampling: boolean indicating whether or not to subsample the data to the minimum number of cells per category
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = []
    catfolders = []
    for sorted_csv_folder in sorted_csv_folders:
        filelist = os.listdir(sorted_csv_folder)
        catlist_exp = []
        catfolders_exp = []
        for f in filelist:
            if os.path.isdir(sorted_csv_folder+'/'+f):
                catlist_exp.append(int(f))
                catfolders_exp.append(sorted_csv_folder+'/'+f)
        # append elements of catlist_exp to catlist if they are not already in catlist
        for i in range(len(catlist_exp)):
            c = catlist_exp[i]
            if c not in catlist:
                catlist.append(c)
                catfolders.append([catfolders_exp[i]])
            else:
                # find index of c in catlist
                idx = catlist.index(c)
                catfolders[idx].append(catfolders_exp[i])
    sort_idx = np.argsort(catlist)
    catlist = np.array(catlist)[sort_idx]
    catfolders = np.array(catfolders)[sort_idx]

    # exclude cells that are missing snaps in one or more channels
    input_files_snapspresent = []
    for c in range(len(cat_names)):

        input_files = list(np.concatenate([glob(os.path.join(x, '*.csv')) for x in catfolders[c]]))
        exppattern = '|'.join(experiments)
        found_nums = [re.search(rf'({exppattern})/.+/{str(catlist[c])}/(\d+)_(\d+)\.csv', f) for f in input_files]
        exp_fnum_and_cell = [[experiments.index(x.group(1)), x.group(2), x.group(3)] for x in found_nums]
        if len(alt_snapsfolders_names) > 0:
            altsnaps_present = [[os.path.exists(f"{alt_snapsfolders[efc[0]]}/{efc[1]}.tif") for efc in exp_fnum_and_cell] for alt_snapsfolders in alt_snapsfolders_list]
            altsnaps_present = np.array(altsnaps_present).all(0)
            input_files_snapspresent.append(np.array(input_files)[altsnaps_present])
        else:
            input_files_snapspresent.append(input_files)

    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    if subsampling:

        # find the number of cells in each category
        ncells = np.zeros(len(catlist))
        for c in range(len(catlist)):
            ncells[c] = len(input_files_snapspresent[c])
        min_cells = np.min(ncells)

        # print subsampling information
        for c in range(len(catlist)):
            print(cat_names[catlist[c]]+': '+str(ncells[c])+' cells')
        print('Subsampling to '+str(min_cells)+' cells per category.')

        # create a list of seeds that will be used to subsample each category
        seedlist = [i*100+i*10+i for i in range(len(catlist))]
            
    SAD = {}

    if settings is None:
        settings = dict(
            likelihood_type = RBME,
            pixel_size_um = 0.16,
            frame_interval = 0.00748,
            focal_depth = 0.7,
            start_frame = 0,
            progress_bar = True,
            sample_size = 1e9
        )

    for c in range(len(cat_names)):
        input_files = input_files_snapspresent[c]

        if subsampling:
            random.seed(seedlist[c])
            input_files = random.sample(input_files, min_cells)

        # Filter out files that have fewer non-singlet trajectories than the user specified threshold
        input_files_filtered = []
        for f in input_files:
            df_tofilter = pd.read_csv(f)
            trajs, counts = np.unique(df_tofilter['trajectory'], return_counts=True)
            if np.sum(counts>1) >= min_trajs_per_cell:
                input_files_filtered.append(f)
        input_files = input_files_filtered

        SAD[catlist[c]] = StateArrayDataset.from_kwargs(pd.DataFrame(input_files, columns=['filepath']),
                                            path_col='filepath',
                                            **settings)
        SAD[catlist[c]].processed_track_statistics.to_csv('track_statistics_cat'+str(catlist[c])+'.csv')
        SAD[catlist[c]].posterior_line_plot(outf+'/posterior_line_plot_cat'+str(catlist[c])+'.png')
        SAD[catlist[c]].posterior_heat_map(outf+'/SAD_heatmap_cat'+str(catlist[c])+'.png')
        
        print('Completed category '+str(cat_names[catlist[c]])+'.')

    with open('SAD_pickle','wb') as fh:
        pickle.dump(SAD, fh)

    return SAD

def bootstrap_SAD(SAD, max_cells_per_replicate = None, nreplicates=1000, cat_names=None, cat_colors=None, outf='Figures', settings=None, boundthresh=0.15):
    '''
    Bootstraps over cells to compute confidence intervals for cell category-specific bound fractions

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        max_cells_per_replicate: Number of cells to sample in each bootstrap replicate. If None, the number of cells in the category with the fewest cells is used.
        nreplicates: Number of samples to draw. Bootstrapping will be performed with the number of replicates specified by this variable, with a number of cells per replicate ranging from 1 to max_cells_per_replicate.
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        settings: dictionary of settings for SASPT state array dataset generation
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    ncells = [SAD[catlist[c]].marginal_posterior_occs.shape[0] for c in range(len(catlist))]
    min_cells = np.min(ncells)
    if max_cells_per_replicate is None:
        max_cells_per_replicate = min_cells

    random.seed(12345)

    mpos_boot_allcats = []

    for c in range(len(catlist)):
        
        # sort rows by bound fraction
        mpo = SAD[catlist[c]].marginal_posterior_occs
        D = SAD[catlist[c]].likelihood.diff_coefs

        mpos_boot_allcellnums = []

        # random sampling with replacement
        for cells_per_replicate in range(1, max_cells_per_replicate+1):

            mpos_boot_ncells = np.zeros((nreplicates, SAD[catlist[c]].marginal_posterior_occs.shape[1]))

            for r in range(nreplicates):
                mpo_choices = random.choices(range(ncells[c]), k = cells_per_replicate)
                mpo_replicate_array = mpo[mpo_choices,:]
                mpo_replicate = np.sum(mpo_replicate_array, axis = 0)
                mpo_replicate_norm = mpo_replicate/mpo_replicate.sum()
                mpos_boot_ncells[r,:] = mpo_replicate_norm

            mpos_boot_allcellnums.append(mpos_boot_ncells)

        mpos_boot_allcats.append(mpos_boot_allcellnums)

        max_occupation = np.max([[np.mean(mpos_boot_ncells, axis = 0) for mpos_boot_ncells in mpos_boot_allcellnums] for mpos_boot_allcellnums in mpos_boot_allcats])
        max_occupation = 1.05*max_occupation
        sel = (D<=boundthresh)
        max_fbound = np.max([[[mpoNorm[sel].sum() for mpoNorm in mpos_boot_ncells] for mpos_boot_ncells in mpos_boot_allcellnums] for mpos_boot_allcellnums in mpos_boot_allcats])
        max_fbound = 1.05*max_fbound

    for c in range(len(catlist)):

        mpos_boot_allcellnums = mpos_boot_allcats[c]

        # compute fbound from the bootstrapped marginal posterior occupations
        mpoNorm_repmean = [np.mean(mpos_boot_ncells, axis = 0) for mpos_boot_ncells in mpos_boot_allcellnums] # mean normalized PDF across replicates for a given number of cells per replicate
        sel = (D<=boundthresh)
        fbound_eachrep = [[mpoNorm[sel].sum() for mpoNorm in mpos_boot_ncells] for mpos_boot_ncells in mpos_boot_allcellnums] # fraction bound for each replicate

        # plotting results
        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(15,5))

        color_ids = np.linspace(0, 1, max_cells_per_replicate)
        colormap = plt.cm.viridis
        colors = [colormap(x) for x in color_ids]
        for i in range(max_cells_per_replicate):
            ax[0].semilogx(D, mpoNorm_repmean[i], c=colors[i])
        sm = plt.cm.ScalarMappable(cmap=colormap, norm=plt.Normalize(vmin=1, vmax=max_cells_per_replicate))
        cbar = fig.colorbar(sm, ax=ax[0])
        cbar.set_label('Number of cells per replicate')
        ax[0].set_xlim(np.min(D), np.max(D))
        ax[0].set_ylim(0, max_occupation)
        ax[0].set_ylabel('Bootstrapped posterior')
        ax[0].set_xlabel('Diffusion coefficient (µm$^2$/s)')

        ax[1].boxplot(fbound_eachrep, vert=True)
        ax[1].set_ylim(0, max_fbound)
        ax[1].set_ylabel('Replicate fbound')
        ax[1].set_xlabel('Number of cells per replicate')

        plt.suptitle(cat_names[catlist[c]])

        fig.tight_layout()

        fig.savefig(outf+'/bootstrapresults_'+cat_names[catlist[c]]+'.png',dpi=300)
        fig.savefig(outf+'/bootstrapresults_'+cat_names[catlist[c]]+'.pdf',dpi=300)
        plt.show()

        print(cat_names[catlist[c]]+':')
        print('Mean fraction bound = '+"{:.3f}".format(np.mean(fbound_eachrep[-1])))
        ci_95_low = np.sort(fbound_eachrep[-1])[math.ceil(nreplicates/100*5)]
        ci_95_high = np.sort(fbound_eachrep[-1])[math.floor(nreplicates/100*95)]
        print('95% confidence interval = '+"{:.3f}".format(ci_95_low)+' - '+"{:.3f}".format(ci_95_high))

    return mpos_boot_allcats

def samplecells(SAD, n_trials = 10, n_cells_per_trial = 10, cat_names=None, cat_colors=None, outf='Figures', settings=None, boundthresh=0.15):
    '''
    Samples a small number of cells from the overall data for each cell category and plots the distribution of bound fractions in these samples

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        n_trials: number of times to draw samples from the population
        n_cells_per_trial: number of cells to sample in each trial
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        settings: dictionary of settings for SASPT state array dataset generation
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    random.seed(12345)

    for c in range(len(catlist)):
            
        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/np.sum(mpo,axis=1)[:,None]
        D = SAD[catlist[c]].likelihood.diff_coefs
        sel = D<boundthresh
        fbounds = np.sum(mpoNorm[:,sel],axis=1)

        mpo_trials = []

        for trial in range(n_trials):
            mpo_trials.append(mpo[random.sample(range(len(mpo)),n_cells_per_trial),:])
        
        mpoNorm_trials = [mpo_trials[i]/np.sum(mpo_trials[i],axis=1)[:,None] for i in range(n_trials)]
        fbound_trials = [np.sum(mpoNorm_trials[i][:,sel],axis=1) for i in range(n_trials)]
        fbound_all_andtrials = [fbounds] + fbound_trials
        labels = ['All cells'] + ['Sample '+str(i+1) for i in range(n_trials)]
        patchcolors = ['lightgray'] + ['white' for i in range(n_trials)]

        fig, ax = plt.subplots(figsize=(10, 5))
        bplot = ax.boxplot(fbound_all_andtrials, labels=labels, patch_artist=True)
        for patch, color in zip(bplot['boxes'], patchcolors):
            patch.set_facecolor(color)
        ax.set_title(cat_names[catlist[c]])
        ax.set_ylabel('Fraction bound')
        fig.savefig(outf+'/sampled_boxplot_'+cat_names[catlist[c]]+'.png',dpi=300)
        fig.savefig(outf+'/sampled_boxplot_'+cat_names[catlist[c]]+'.pdf',dpi=300)
        plt.show()

        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(10,6))

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        mpoNorm_trials_toplot = np.concatenate(mpoNorm_trials, axis=0)
        ax[0].imshow(mpoNorm_trials_toplot,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].set_yticks(np.arange(n_trials)*n_cells_per_trial)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        spectra_ids = [np.zeros(mpoNorm_trials[i].shape[0])+i+1 for i in range(n_trials)]
        spectra_ids_toplot = np.concatenate(spectra_ids, axis=0)
        spectra_ids_toplot = spectra_ids_toplot[:,np.newaxis]
        ax[1].imshow(spectra_ids_toplot, cmap='tab10', aspect=0.5)
        ax[1].invert_yaxis()
        ax[1].get_xaxis().set_visible(False)
        ax[1].get_yaxis().set_visible(False)
        ax[1].yaxis.tick_right()
        ax[1].margins(x=0, y=0)
        ax[1].spines['bottom'].set_color('lightgray')
        ax[1].spines['top'].set_color('lightgray')
        ax[1].spines['right'].set_color('lightgray')
        ax[1].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=1.2)
        fig.tight_layout()

        fig.savefig(outf+'/sample_heatmaps_'+cat_names[catlist[c]]+'.png',dpi=300)
        fig.savefig(outf+'/sample_heatmaps_'+cat_names[catlist[c]]+'.pdf',dpi=300)
        plt.show()


def get_ND2_time(fname):
        
    """
    Function by Thomas Graham
    Get timestamp in Julian Date Number from a Nikon nd2 file.
    
    Inputs:
        fname  :   string - path to the nd2 file
        
    Returns:
        Julian Date Number as a floating point number 
    
    """
    
    bytesfromend = int(1e6) # where to start reading in the metadata relative the end of the file
    
    # Julian Date Number pattern to match in the metadata at the end of the ND2 file
    pattern = r"<ModifiedAtJDN runtype=\"double\" value=\"(.+?)\"/>" 

    with open(fname, "rb") as file:
        # Read the bytes at the end of the file
        file.seek(os.path.getsize(fname)-bytesfromend)
        bytes = file.read()
        decoded = bytes.decode(errors='ignore')
        numbers = re.findall(pattern, decoded)
        if not numbers:
            return float('nan')
        else:
            return float(numbers[0])

def package_cellstats(SAD, experiments, sorted_csv_folders, snapsfolders, alt_snapsfolders_names, alt_snapsfolders_list, roi_measurements_folders, maskmats, maskvarnames, ccmarker_folders, outf='Figures', savefigs=False, boundthresh=0.15):

    '''
    Writes out a CSV file for each cell type (category) containing data regarding the cells that belong to that category.
    Also returns a dictionary (cellstats) containing the data in all the CSV files. The category IDs are the keys for each dictionary entry.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        experiments: list of paths to the parent folders of each experiment
        sorted_csv_folders: list of paths (for each experiment) to cell-specific tracking data organized into sub-directories for each cell category
        snapsfolders: list of paths (for each experiment) to directories containing images used to create masks
        alt_snapsfolders_names: list of names of alternative channels to be analyzed
        alt_snapsfolders_list: list of paths (for each experiment) to directories containing images from alternative channels
        roi_measurements_folders: list of paths (for each experiment) to directories containing a CSV file for each ROI that describes the features of each cell in the ROI
        maskmats: list of path (for each experiment) to output .mat files from CellPicker containing the masks and category IDs for each cell
        maskvarnames: list of the names of the variable (for each experiment) in the corresponding .mat file that contains the masks
        ccmarker_folders: list of paths (for each experiment) to directories containing images in the DHB-GFP channel used to identify cell cycle stages
        outf: path to figure output directory
        savefigs: boolean indicating whether or not to save images of the nuclear and cytoplasmic ring masks used to determine cell cycle stages
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)

    Returns:
        a dictionary of pandas DataFrames (for each cell category) containing morphological, intensity-based, and diffusivity-based data for each cell
    '''

    cellstats = {}
    catlist = list(SAD.keys())

    for c in range(len(catlist)):

        file_cell_num = []
        exppattern = '|'.join(experiments)
        pattern = rf'({exppattern})/.+/{str(catlist[c])}/(\d+)_(\d+)\.csv'

        cellstats[catlist[c]] = pd.DataFrame()

        for fname in SAD[catlist[c]].condition_map.keys():
            match = re.search(pattern, fname)

            # get file number and cell number for each input file
            exp = experiments.index(match.group(1))
            fnum = int(match.group(2))
            cellnum = int(match.group(3))

            # open the corresponding cell parameter file
            params = pd.read_csv(f'{roi_measurements_folders[exp]}/{fnum}.csv',index_col=0)
            params['experiment_idx'] = exp
            params['experiment_folder'] = match.group(1)
            params['filenum'] = fnum
            params['cellnum'] = cellnum

            currdf = pd.DataFrame([params.loc[cellnum].values], columns=params.columns)

            # append to dataframe 
            # cellstats[catlist[c]] = cellstats[catlist[c]].append(currdf,ignore_index=True)
            cellstats[catlist[c]] = pd.concat([cellstats[catlist[c]], currdf], axis=0, ignore_index=True)

        # Get input file list again.
        input_files = SAD[catlist[c]].condition_map.keys()
        found_nums = [re.search(rf'({exppattern})/.+/{str(catlist[c])}/(\d+)_(\d+)\.csv', f) for f in input_files]
        exp_fnum_and_cell = [[experiments.index(x.group(1)), x.group(2), x.group(3)] for x in found_nums]

        # Load masks
        masks = []
        for maskmat in maskmats:
            masks.append(scipy.io.loadmat(maskmat))

        # For each input file, parse the numbers, and get the corresponding mask for each input file
        # Note that file number indexing starts with 1, but Python array indexing starts with 0,
        # so it is necessary to subtract 1 from the file number index to get the corresponding array index.
        masks_by_IF = [masks[exp][maskvarnames[exp]][0][int(i)-1]==int(j) for [exp,i,j] in exp_fnum_and_cell]

        # TEMPORARY FIX BECAUSE ROIMASKS FROM CELLPICKER ARE 1 PIXEL TOO BIG IN BOTH DIMENSIONS
        masks_by_IF = [masks_by_IF[mnum][1:,1:] if maskvarnames[exp]=='roimasks' else masks_by_IF[mnum] for mnum,[exp,i,j] in enumerate(exp_fnum_and_cell)]

        # get mask area
        areas = [x.sum() for x in masks_by_IF]
        # determine whether or not it is on the boundary
        on_boundary = [im[0,:].any() or im[-1,:].any() or im[:,0].any() or im[:,-1].any() for im in masks_by_IF]
        not_on_boundary = [not x for x in on_boundary]

        # Measure properties of the mask - area and mean intensity in the snaps images
        intensities = np.zeros(len(exp_fnum_and_cell))
        for i, efc in enumerate(exp_fnum_and_cell):
            mask = masks_by_IF[i]
            im = np.array(Image.open(f"{snapsfolders[efc[0]]}/{efc[1]}.tif"))
            intensities[i] = np.mean(im[mask > 0])

        # Measure the mean intensity in other channels
        if len(alt_snapsfolders_names) > 0:
            alt_intensities = np.zeros((len(exp_fnum_and_cell), len(alt_snapsfolders_names)))
            for i, efc in enumerate(exp_fnum_and_cell):
                mask = masks_by_IF[i]
                for j, alt_snapsfolders in enumerate(alt_snapsfolders_list):
                    if len(alt_snapsfolders[efc[0]]) > 0:
                        im = np.array(Image.open(f"{alt_snapsfolders[efc[0]]}/{efc[1]}.tif"))
                        alt_intensities[i, j] = np.mean(im[mask > 0])
                    else:
                        alt_intensities[i, j] = np.nan

        # Adding cell cycle information
        if len(ccmarker_folders) > 0:
            # make output directory
            os.makedirs(outf, exist_ok = True)
            os.makedirs(outf+'/cell_cycle', exist_ok = True)
            cellcycle_ncratio = np.zeros(len(exp_fnum_and_cell))
            for i, efc in enumerate(exp_fnum_and_cell):
                mask = masks_by_IF[i]
                mask = mask.astype(int)
                im_ccm = np.array(Image.open(f"{ccmarker_folders[efc[0]]}/{efc[1]}.tif"))
                if savefigs:
                    stats = calculate_nuclear_ring_intensities(mask, im_ccm, show_plot=False, out_png=f'{outf}/cell_cycle/{str(efc[0])}_{str(efc[1])}_{str(efc[2])}')
                else:
                    stats = calculate_nuclear_ring_intensities(mask, im_ccm, show_plot=False)
                cellcycle_ncratio[i] = stats['nuclear_ring_ratio'][0]

        # adding fraction bound and boundary proximity information
        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
        D = SAD[catlist[c]].likelihood.diff_coefs
        sel = (D<=boundthresh)
        fbound = mpoNorm[:,sel].sum(1)
        D_peak = D[np.argmax(mpoNorm, axis=1)]
        cellstats[catlist[c]] = cellstats[catlist[c]].assign(fbound=fbound)
        cellstats[catlist[c]] = cellstats[catlist[c]].assign(not_on_boundary=not_on_boundary)
        cellstats[catlist[c]] = cellstats[catlist[c]].assign(area_mask=areas)
        cellstats[catlist[c]] = cellstats[catlist[c]].assign(intensity_mask=intensities)
        cellstats[catlist[c]] = cellstats[catlist[c]].assign(D_peak=D_peak)

        if len(ccmarker_folders) > 0:
            cellstats[catlist[c]] = cellstats[catlist[c]].assign(cellcycle_ncratio=cellcycle_ncratio)

        if len(alt_snapsfolders_names) > 0:
            for j, alt_snapsfolders_name in enumerate(alt_snapsfolders_names):
                eval('cellstats[catlist[c]].insert(loc=cellstats[catlist[c]].shape[1], column="intensity_mask_'+alt_snapsfolders_name+'", value=alt_intensities[:, j])')

        # write out to a csv file for later reference
        cellstats[catlist[c]].to_csv('cellstats_cat'+str(catlist[c])+'.csv')

    return cellstats

def get_exp_stats(SAD, cellstats, experiments, default_channel_name, alt_snapsfolders_names, cat_names=None):

    '''
    Prints statistics for each cell type (category) and experiment.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        experiments: list of paths to the parent folders of each experiment
        default_channel_name: name of the channel used to generate cell masks
        alt_snapsfolders_names: list of names of alternative channels to be analyzed
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys

    Returns:
        pandas DataFrame containing mean morphological, intensity-based, diffusivity-based, and tracking-based data for each cell category
    '''

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))

    indices_list = [["Number of "+cat_names[c]+" cells", "Number of non-boundary "+cat_names[c]+" cells",
                     "Mean nuclear area of "+cat_names[c]+" cells (µm\u00b2)", "Mean nuclear area of non-boundary "+cat_names[c]+" cells (µm\u00b2)",
                     "Mean nuclear eccentricity of "+cat_names[c]+" cells", "Mean nuclear eccentricity of non-boundary "+cat_names[c]+" cells",
                     f"Mean {default_channel_name} intensity of nuclei in "+cat_names[c]+" cells (AU)", "Mean fraction bound in "+cat_names[c]+" cells",
                     "Mean fraction bound in non-boundary "+cat_names[c]+" cells",
                     "Mean diffusivity peak in "+cat_names[c]+" cells (µm\u00b2/s)",
                     "Mean diffusivity peak in non-boundary "+cat_names[c]+" cells (µm\u00b2/s)",
                     "Mean number of tracks in "+cat_names[c]+" cells",
                     "Mean number of jumps in "+cat_names[c]+" cells",
                     "Mean number of detections in "+cat_names[c]+" cells"] for c in catlist]
    if len(alt_snapsfolders_names) > 0:
        for alt_snapsfolders_name in alt_snapsfolders_names:
            for cid,c in enumerate(catlist):
                indices_list[cid].append("Mean " + alt_snapsfolders_name + " intensity of nuclei in "+cat_names[c]+" cells (AU)")
    indices = [item for sublist in indices_list for item in sublist]
    exp_data_all = []

    for exp_id in range(len(experiments)):

        exp = experiments[exp_id]
        exp_data = []

        for c in catlist:

            df_nonboundary = cellstats[c].loc[(cellstats[c]['experiment_folder']==exp) & (cellstats[c]['not_on_boundary']==True)]

            exp_data.append(len(cellstats[c].loc[cellstats[c]['experiment_folder']==exp]))
            exp_data.append(np.sum(list(cellstats[c].loc[cellstats[c]['experiment_folder']==exp]['not_on_boundary'])))
            exp_data.append(np.mean(cellstats[c].loc[cellstats[c]['experiment_folder']==exp]['area'])*(SAD[c].likelihood.pixel_size_um)**2)
            exp_data.append(np.mean(df_nonboundary['area'])*(SAD[c].likelihood.pixel_size_um)**2)
            exp_data.append(np.mean(cellstats[c].loc[cellstats[c]['experiment_folder']==exp]['eccentricity']))
            exp_data.append(np.mean(df_nonboundary['eccentricity']))
            exp_data.append(np.mean(cellstats[c].loc[cellstats[c]['experiment_folder']==exp]['intensity_mask']))
            exp_data.append(np.mean(cellstats[c].loc[cellstats[c]['experiment_folder']==exp]['fbound']))
            exp_data.append(np.mean(df_nonboundary['fbound']))
            exp_data.append(np.mean(cellstats[c].loc[cellstats[c]['experiment_folder']==exp]['D_peak']))
            exp_data.append(np.mean(df_nonboundary['D_peak']))

            exp_list = list(SAD[c].processed_track_statistics['filepath'])
            exp_pattern = exp + '/'
            SAD_exp_match = [i for i in range(len(exp_list)) if exp_pattern in exp_list[i]]
            SAD_subset = SAD[c].processed_track_statistics.loc[SAD_exp_match]
            exp_data.append(np.mean(SAD_subset['n_tracks']))
            exp_data.append(np.mean(SAD_subset['n_jumps']))
            exp_data.append(np.mean(SAD_subset['n_detections']))

            if len(alt_snapsfolders_names) > 0:
                for alt_snapsfolders_name in alt_snapsfolders_names:
                    exp_data.append(np.mean(cellstats[c].loc[cellstats[c]['experiment_folder']==exp]['intensity_mask_'+alt_snapsfolders_name]))

        exp_data_all.append(exp_data)

    exp_data_all_array = np.transpose(np.array(exp_data_all))
    expstats = pd.DataFrame(data=exp_data_all_array, index=indices, columns=["Experiment "+str(x) for x in range(len(experiments))])
    return expstats

def print_mean_fbound_cellavg(SAD, cellstats, cat_names=None):
    
    '''
    Prints the mean fraction bound for each cell type, averaged over all cells belonging to that cell type.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
    '''

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    
    print('Mean fraction bound in all cells = ' + "{:.3f}".format(np.mean(np.concatenate([list(cellstats[c]['fbound']) for c in catlist]))))

    for c in catlist:
        print('Mean fraction bound in '+cat_names[c]+' cells = '+"{:.3f}".format(np.mean(cellstats[c]['fbound'])))

def print_mean_fbound_noavg(SAD, settings=None, cat_names=None, boundthresh=0.15):
    '''
    Prints the mean fraction bound for each cell type, with the fraction bound computed
    by pooling trajectories from all cells of a particular type.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        settings: dictionary of settings for SASPT state array dataset generation
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
    '''

    catlist = list(SAD.keys())

    if settings is None:
        settings = dict(
            likelihood_type = RBME,
            pixel_size_um = 0.16,
            frame_interval = 0.00748,
            focal_depth = 0.7,
            start_frame = 0,
            progress_bar = True,
            sample_size = 1e9
        )
    
    input_files = list(np.concatenate([list(SAD[c].paths['filepath']) for c in catlist]))
    SAD_allcats = StateArrayDataset.from_kwargs(pd.DataFrame(input_files, columns=['filepath']),
                                                path_col='filepath',
                                                **settings)
    mpo_c = SAD_allcats.infer_posterior_by_condition(col='default_condition')
    mpo = mpo_c[0][0]
    mpoNorm = mpo/mpo.sum()
    D = SAD_allcats.likelihood.diff_coefs
    sel = (D<=boundthresh)
    fbound = mpoNorm[sel].sum()
    print('Fraction bound in all cells = ' + "{:.3f}".format(fbound))

    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))

    for c in catlist:
        mpo_c = SAD[c].infer_posterior_by_condition(col='default_condition')
        mpo = mpo_c[0][0]
        mpoNorm = mpo/mpo.sum()
        D = SAD[c].likelihood.diff_coefs
        sel = (D<=boundthresh)
        fbound = mpoNorm[sel].sum()
        print('Fraction bound in '+cat_names[c]+' cells = '+ "{:.3f}".format(fbound))

def plot_D_spectra(SAD, cat_names=None, cat_colors=None, outf='Figures', separate_cats=False):

    '''
    Generates plots of the diffusion spectra (normalized marginal posterior occupations) of individual cells belonging to different categories

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        separate_cats: boolean indicating whether or not data from different cell categories should be plotted separately
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    if not separate_cats:
        cmapchoice = plt.cm.tab20
        plt.figure()
        for c in range(len(catlist)):
            D = SAD[catlist[c]].likelihood.diff_coefs
            mpo = SAD[catlist[c]].marginal_posterior_occs
            for j in range(mpo.shape[0]):
                plt.semilogx(D, mpo[j]/mpo[j].sum(), c=cat_colors[catlist[c]])
            plt.semilogx([],[],c=cat_colors[catlist[c]], label=cat_names[catlist[c]])
        plt.ylim([0,.1])
        plt.xlim([0.01,100])
        # plt.gca().set_xticks(plt.gca().get_xticks()[1:-1], ["{:.2f}".format(x) if x<0.1 else "{:.1f}".format(x) if (x<1) else "{:.0f}".format(x) for x in plt.gca().get_xticks()[1:-1]])
        plt.xlabel('Diffusion coefficient (µm$^2$/s)')
        plt.ylabel ('Fraction of molecules')
        plt.legend()
        plt.savefig(outf+'/probability_distribution_function_allcats.png',dpi=300)
        plt.savefig(outf+'/probability_distribution_function_allcats.pdf',dpi=300)
        plt.show()

    else:
        for c in range(len(catlist)):
            D = SAD[catlist[c]].likelihood.diff_coefs
            mpo = SAD[catlist[c]].marginal_posterior_occs
            plt.figure()
            for j in range(mpo.shape[0]):
                plt.semilogx(D,mpo[j]/mpo[j].sum())
            plt.ylim([0,.1]);
            plt.xlim([0.01,100]);
            # plt.gca().set_xticks(plt.gca().get_xticks()[1:-1], ["{:.2f}".format(x) if x<0.1 else "{:.1f}".format(x) if (x<1) else "{:.0f}".format(x) for x in plt.gca().get_xticks()[1:-1]])
            plt.xlabel('Diffusion coefficient (µm$^2$/s)');
            plt.ylabel ('Fraction of molecules');
            plt.title(cat_names[catlist[c]])
            plt.savefig(outf+'/probability_distribution_function_'+cat_names[catlist[c]]+'.png',dpi=300)
            plt.savefig(outf+'/probability_distribution_function_'+cat_names[catlist[c]]+'.pdf',dpi=300)
            plt.show()

def plot_CDF_spectra(SAD, cat_names=None, cat_colors=None, outf='Figures', separate_cats=False):

    '''
    Generates plots of the cumulative diffusion spetra of individual cells belonging to different categories

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        separate_cats: boolean indicating whether or not data from different cell categories should be plotted separately
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    if not separate_cats:
        cmapchoice = plt.cm.tab20
        plt.figure()
        for c in range(len(catlist)):
            D = SAD[catlist[c]].likelihood.diff_coefs
            mpo = SAD[catlist[c]].marginal_posterior_occs
            for j in range(mpo.shape[0]):
                plt.semilogx(D, mpo[j].cumsum()/mpo[j].sum(), c=cat_colors[catlist[c]])
            plt.semilogx([],[],c=cat_colors[catlist[c]], label=cat_names[catlist[c]])
        plt.ylim([0,1]);
        plt.xlim([0.01,100]);
        # plt.gca().set_xticks(plt.gca().get_xticks()[1:-1], ["{:.2f}".format(x) if x<0.1 else "{:.1f}".format(x) if (x<1) else "{:.0f}".format(x) for x in plt.gca().get_xticks()[1:-1]])
        plt.xlabel('Diffusion coefficient (µm$^2$/s)');
        plt.ylabel ('Fraction of molecules');
        plt.legend()
        plt.savefig(outf+'/cumulative_distribution_function_allcats.png',dpi=300)
        plt.savefig(outf+'/cumulative_distribution_function_allcats.pdf',dpi=300)
        plt.show()

    else:
        for c in range(len(catlist)):
            D = SAD[catlist[c]].likelihood.diff_coefs
            mpo = SAD[catlist[c]].marginal_posterior_occs
            plt.figure()
            for j in range(mpo.shape[0]):
                plt.semilogx(D,mpo[j].cumsum()/mpo[j].sum())
            plt.ylim([0,1]);
            plt.xlim([0.01,100]);
            # plt.gca().set_xticks(plt.gca().get_xticks()[1:-1], ["{:.2f}".format(x) if x<0.1 else "{:.1f}".format(x) if (x<1) else "{:.0f}".format(x) for x in plt.gca().get_xticks()[1:-1]])
            plt.xlabel('Diffusion coefficient (µm$^2$/s)');
            plt.ylabel ('Fraction of molecules');
            plt.title(cat_names[catlist[c]])
            plt.savefig(outf+'/cumulative_distribution_function_'+cat_names[catlist[c]]+'.png',dpi=300)
            plt.savefig(outf+'/cumulative_distribution_function_'+cat_names[catlist[c]]+'.pdf',dpi=300)
            plt.show()

def plot_D_heatmap(SAD, cat_names=None, cat_colors=None, outf='Figures'):
    '''
    For each cell category, generates a heatmap showing the distribution of inferred distribution coefficients for each cell.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    for c in range(len(catlist)):

        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row

        input_files = SAD[catlist[c]].condition_map.keys()
        found_nums = [re.search(r'(\d+)_(\d+)',os.path.basename(f)) for f in input_files]
        fnum_and_cell = [[x.group(1),x.group(2)] for x in found_nums]
        # actual order of the movies
        sortorder = np.argsort([int(i) for i,j in fnum_and_cell])

        fig,ax = plt.subplots(1,1)
        # reorder rows by the actual order in which movies were collected, not the alphabetical order
        # I'm not sure if this is necessary but it can't hurt
        ax.imshow(mpoNorm[sortorder,:],vmin=0,vmax=0.04) 
        ax.set_xticks([0,24,49,74,99]);
        ax.set_xticklabels([r'$0.01$',r'$0.1$','1',r'$10$',r'$100$']);
        ax.set_xlabel('Diffusion coefficient (µm$^2$/s)')
        ax.set_ylabel('Cell number');
        plt.title(cat_names[catlist[c]])
        ax.invert_yaxis()
        ax.margins(x=0, y=0)

        fig.savefig(outf+'/heatmap_diffusioncoefficient_unsorted_'+cat_names[catlist[c]]+'.png',dpi=300)
        fig.savefig(outf+'/heatmap_diffusioncoefficient_unsorted_'+cat_names[catlist[c]]+'.pdf',dpi=300)
        plt.show()

def plot_mean_PDF_unweighted(SAD, cat_names=None, cat_colors=None, outf='Figures', separate_cats=False):
    '''
    Plot mean diffusion spectrum - not weighted by number of trajectories per cell (i.e., equal weight per cell)

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        separate_cats: boolean indicating whether or not data from different cell categories should be plotted separately
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    if not separate_cats:

        fig,ax = plt.subplots(1,1)
        cmapchoice = plt.cm.tab20

        for c in range(len(catlist)):
            
            D = SAD[catlist[c]].likelihood.diff_coefs
            mpo = SAD[catlist[c]].marginal_posterior_occs
            ax.semilogx(D,mpo.sum(0)/mpo.sum(0).sum(),linewidth=2,
                        c=cat_colors[catlist[c]], label=cat_names[catlist[c]])
            ax.set_xlabel('Diffusion coefficient (µm$^2$/s)')
            ax.set_ylabel('Fraction of molecules')
            ax.set_ylim([0,0.1])
            ax.set_xlim([0.01,100])
            ax.set_xticks([.01,.1,1,10,100])
            ax.set_xticklabels(["0.01", "0.1", "1", "10", "100"])
        plt.legend()
        fig.savefig(outf+'/mean_PDF_unweighted_allcats.png',dpi=300)
        fig.savefig(outf+'/mean_PDF_unweighted_allcats.pdf',dpi=300)
        plt.show()

    else:

        for c in range(len(catlist)):
            D = SAD[catlist[c]].likelihood.diff_coefs
            mpo = SAD[catlist[c]].marginal_posterior_occs
            mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
            fig,ax = plt.subplots(1,1)
            [ax.semilogx(D,j,color=cat_colors[catlist[c]], alpha=0.2,linewidth=0.5) for j in mpoNorm/mpoNorm.sum(1)[:,np.newaxis]];
            ax.semilogx(D,mpoNorm.sum(0)/mpoNorm.sum(0).sum(),color=cat_colors[catlist[c]],linewidth=3)
            ax.set_xlabel('Diffusion coefficient (µm$^2$/s)');
            ax.set_ylabel('Fraction of molecules')
            plt.title(cat_names[catlist[c]])
            ax.set_ylim([0,0.1])
            ax.set_xlim([0.01,100])
            ax.set_xticks([.01,.1,1,10,100])
            ax.set_xticklabels(["0.01", "0.1", "1", "10", "100"])
            fig.savefig(outf+'/mean_PDF_unweighted_'+cat_names[catlist[c]]+'.png',dpi=300)
            fig.savefig(outf+'/mean_PDF_unweighted_'+cat_names[catlist[c]]+'.pdf',dpi=300)
            plt.show()

def plot_mean_CDF_unweighted(SAD, cat_names=None, cat_colors=None, outf='Figures', separate_cats=False):
    '''
    Plot mean cumulative diffusion spectrum - not weighted by number of trajectories per cell (i.e., equal weight per cell)

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        separate_cats: boolean indicating whether or not data from different cell categories should be plotted separately
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    if not separate_cats:

        fig,ax = plt.subplots(1,1)
        cmapchoice = plt.cm.tab20

        for c in range(len(catlist)):
            
            D = SAD[catlist[c]].likelihood.diff_coefs
            mpo = SAD[catlist[c]].marginal_posterior_occs
            mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
            ax.semilogx(D,mpoNorm.sum(0).cumsum()/mpoNorm.sum(0).sum(),linewidth=2,
                        c=cat_colors[catlist[c]], label=cat_names[catlist[c]])
            ax.set_xticks([.01,.1,1,10,100])
            ax.set_xticklabels(["0.01", "0.1", "1", "10", "100"])
            ax.set_xlabel('Diffusion coefficient (µm$^2$/s)')
            ax.set_ylabel('Fraction of molecules')
            ax.set_ylim([0,1])
            ax.set_xlim([0.01,100])
        plt.legend()
        fig.savefig(outf+'/mean_CDF_unweighted_allcats.png',dpi=300)
        fig.savefig(outf+'/mean_CDF_unweighted_allcats.pdf',dpi=300)
        plt.show()

    else:

        for c in range(len(catlist)):
            D = SAD[catlist[c]].likelihood.diff_coefs
            mpo = SAD[catlist[c]].marginal_posterior_occs
            mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
            fig,ax = plt.subplots(1,1)
            [ax.semilogx(D,j.cumsum(),color=cat_colors[catlist[c]], alpha=0.2, linewidth=0.5) for j in mpoNorm/mpoNorm.sum(1)[:,np.newaxis]];
            ax.semilogx(D,mpoNorm.sum(0).cumsum()/mpoNorm.sum(0).sum(),color=cat_colors[catlist[c]],linewidth=3)
            ax.set_xticks([.01,.1,1,10,100])
            ax.set_xticklabels(["0.01", "0.1", "1", "10", "100"])
            ax.set_xlabel('Diffusion coefficient (µm$^2$/s)');
            ax.set_ylabel('Fraction of molecules')
            plt.title(cat_names[catlist[c]])
            ax.set_ylim([0,1])
            ax.set_xlim([0.01,100])
            fig.savefig(outf+'/mean_CDF_unweighted_'+cat_names[catlist[c]]+'.png',dpi=300)
            fig.savefig(outf+'/mean_CDF_unweighted_'+cat_names[catlist[c]]+'.pdf',dpi=300)
            plt.show()

def plot_state_arrays(SAD, cat_names=None, cat_colors=None, outf='Figures'):
    '''
    Plot state arrays for each cell type/category using data from all chosen cells.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    for c in range(len(catlist)):
        po = SAD[catlist[c]].posterior_occs
        mean_2D_dspect = po.sum(0)/po.sum()
        plt.figure()
        plt.imshow(mean_2D_dspect.T,vmin=0,vmax=0.001)
        plt.xlabel('$D$ (µm$^2$/s)')
        plt.ylabel('Localization error (nm)')
        plt.gca().invert_yaxis()
        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']
        plt.xticks(xticks,xticklabel)
        yticks = np.array([0,18,36])
        yticklabel = ['0','35','70'] # assumes that you're using the default state array of localization errors
        plt.yticks(yticks,yticklabel);
        plt.title(cat_names[catlist[c]])
        plt.margins(x=0, y=0)
        plt.savefig(outf+'/statearray_'+cat_names[catlist[c]]+'.png',dpi=300)
        plt.savefig(outf+'/statearray_'+cat_names[catlist[c]]+'.pdf',dpi=300)
        plt.show()

def plot_D_heatmap_ranked_fbound(SAD, cat_names=None, cat_colors=None, outf='Figures', boundthresh = 0.15):
    '''
    For each cell category, generates a heatmap showing the distribution of inferred diffusion coefficients for each cell. Rank cells in the
    heatmap according to the fraction of bound trajectories. Also plots the fraction of bound trajectories for each cell, the PDFs, and the
    distribution of bound fractions.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    for c in range(len(catlist)):

        # sort rows by bound fraction
        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
        D = SAD[catlist[c]].likelihood.diff_coefs
        sel = (D<=boundthresh)
        fbound = mpoNorm[:,sel].sum(1)
        sortorder = np.argsort(fbound)

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        # fig,ax = plt.subplots(nrows=2,ncols=2,sharey='row',sharex='col',figsize=(6,8))
        fig,ax = plt.subplots(nrows=2,ncols=2,sharey=False,sharex=False,figsize=(9,6))
        plt.suptitle(cat_names[catlist[c]])
        ax[0,0].imshow(mpoNorm[sortorder,:],vmin=0,vmax=0.04,aspect='auto')
        ax[0,0].set_ylabel('Cell number')
        ax[0,0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0,0].set_xticks(xticks)
        ax[0,0].set_xticklabels(xticklabel)
        ax[0,0].invert_yaxis()
        ax[0,0].margins(x=0, y=0)


        ax[0,1].plot(fbound[sortorder],range(len(fbound)),'-', c=cat_colors[catlist[c]])
        ax[0,1].set_ylim(0,len(fbound)-1)
        ax[0,1].set_xlim(0,1)
        ax[0,1].set_ylabel('Cell number')
        ax[0,1].set_xlabel('Fraction bound')

        # comment the following line out if you don't want the overlay of all single-cell diffusion spectra
        [ax[1,0].semilogx(D,j,color=cat_colors[catlist[c]], alpha=0.5,linewidth=0.5) for j in mpo/mpo.sum(1)[:,np.newaxis]];
        ax[1,0].semilogx(D,mpo.sum(0)/mpo.sum(0).sum(),color=cat_colors[catlist[c]],linewidth=2)
        ax[1,0].set_xticks([.01,.1,1,10,100])
        ax[1,0].set_xticklabels(['0.01','0.1','1','10','100'])
        ax[1,0].set_xlim([0.01,100])
        ax[1,0].set_ylim([0,0.06])
        ax[1,0].plot([boundthresh,boundthresh],ax[1,0].get_ylim(),'--',color=[.5,.5,.5])
        ax[1,0].set_xlabel('$D$ (µm$^2$/s)')
        ax[1,0].set_ylabel('Fraction of molecules')

        ax[1,1].hist(fbound,color=cat_colors[catlist[c]])
        ax[1,1].set_xlim(0,1)
        ax[1,1].set_xlabel('Fraction bound')
        ax[1,1].set_ylabel('Number of cells')

        fig.subplots_adjust(wspace=.4, hspace=.25)

        fig.savefig(outf+'/combo_plot_ranked_fbound_'+cat_names[catlist[c]]+'.png',dpi=300)
        fig.savefig(outf+'/combo_plot_ranked_fbound_'+cat_names[catlist[c]]+'.pdf',dpi=300)
        plt.show()

def plot_D_heatmap_ranked_area_allcells(SAD, cellstats, cat_names=None, cat_colors=None, outf='Figures', boundthresh = 0.15):
    '''
    For each cell category, generates a heatmap showing the distribution of inferred diffusion coefficients for each cell. Rank cells in the
    heatmap according to their areas. Also plots the area of each cell, the PDFs, and the distribution of areas.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    max_area = np.max([np.max(cellstats[i]['area_mask'].to_numpy()*(SAD[i].likelihood.pixel_size_um)**2) for i in list(cellstats.keys())])

    for c in range(len(catlist)):

        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
        D = SAD[catlist[c]].likelihood.diff_coefs
        areas = cellstats[catlist[c]]['area_mask'].to_numpy()
        areas = areas*(SAD[catlist[c]].likelihood.pixel_size_um)**2
        sortorder = np.argsort(areas)

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        # fig,ax = plt.subplots(nrows=2,ncols=2,sharey='row',sharex='col',figsize=(6,8))
        fig,ax = plt.subplots(nrows=2,ncols=2,sharey=False,sharex=False,figsize=(9,6))
        plt.suptitle(cat_names[catlist[c]])
        ax[0,0].imshow(mpoNorm[sortorder,:],vmin=0,vmax=0.04,aspect='auto')
        ax[0,0].set_ylabel('Cell number')
        ax[0,0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0,0].set_xticks(xticks)
        ax[0,0].set_xticklabels(xticklabel)
        ax[0,0].invert_yaxis()
        ax[0,0].margins(x=0, y=0)


        ax[0,1].plot(np.array(areas)[sortorder],range(len(areas)),'-', c=cat_colors[catlist[c]])
        # ax[1,1].set_yticks([])
        ax[0,1].set_ylim(0,len(areas)-1)
        ax[0,1].set_xlim(0,max_area*1.05)
        ax[0,1].set_ylabel('Cell number')
        ax[0,1].set_xlabel('Area (µm$^2$)')

        # comment the following line out if you don't want the overlay of all single-cell diffusion spectra
        [ax[1,0].semilogx(D,j,color=cat_colors[catlist[c]], alpha=0.5,linewidth=0.5) for j in mpo/mpo.sum(1)[:,np.newaxis]];
        ax[1,0].semilogx(D,mpo.sum(0)/mpo.sum(0).sum(),color=cat_colors[catlist[c]],linewidth=2)
        ax[1,0].set_xticks([.01,.1,1,10,100])
        ax[1,0].set_xticklabels(['0.01','0.1','1','10','100'])
        ax[1,0].set_ylim([0,0.06])
        ax[1,0].set_xlim([0.01,100])
        ax[1,0].plot([boundthresh,boundthresh],ax[1,0].get_ylim(),'--',color=[.5,.5,.5])
        ax[1,0].set_xlabel('$D$ (µm$^2$/s)')
        ax[1,0].set_ylabel('Fraction of molecules')

        ax[1,1].hist(areas,color=cat_colors[catlist[c]],bins=np.linspace(0, max_area, 25))
        ax[1,1].set_xlim(ax[0,1].get_xlim())
        ax[1,1].set_xlabel('Area (µm$^2$)')
        ax[1,1].set_ylabel('Number of cells')

        fig.subplots_adjust(wspace=.4, hspace=.25)

        fig.savefig(outf+'/combo_plot_ranked_area_allcells_'+cat_names[catlist[c]]+'.png',dpi=300)
        fig.savefig(outf+'/combo_plot_ranked_area_allcells_'+cat_names[catlist[c]]+'.pdf',dpi=300)
        plt.show()

def plot_D_heatmap_ranked_area_nonboundarycells(SAD, cellstats, cat_names=None, cat_colors=None, outf='Figures', boundthresh = 0.15):
    '''
    For each cell category, generates a heatmap showing the distribution of inferred diffusion coefficients for each cell. Rank cells in the
    heatmap according to their areas. Also plots the area of each cell, the PDFs, and the distribution of areas. Only include cells that are not
    at the ROI boundaries.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    max_area = np.max([np.max(cellstats[i]['area_mask'].to_numpy()*(SAD[i].likelihood.pixel_size_um)**2) for i in list(cellstats.keys())])

    for c in range(len(catlist)):

        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
        D = SAD[catlist[c]].likelihood.diff_coefs
        areas = cellstats[catlist[c]]['area_mask'].to_numpy()
        areas = areas*(SAD[catlist[c]].likelihood.pixel_size_um)**2
        not_on_boundary = list(cellstats[catlist[c]]['not_on_boundary'])
        areas_not_boundary = np.asarray(areas)[not_on_boundary]
        mpo_not_boundary = mpo[not_on_boundary]
        mpoNorm_not_boundary = mpoNorm[not_on_boundary]
        sortorder = np.argsort(areas_not_boundary)

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        # fig,ax = plt.subplots(nrows=2,ncols=2,sharey='row',sharex='col',figsize=(6,8))
        fig,ax = plt.subplots(nrows=2,ncols=2,sharey=False,sharex=False,figsize=(9,6))
        plt.suptitle(cat_names[catlist[c]])
        ax[0,0].imshow(mpoNorm_not_boundary[sortorder,:],vmin=0,vmax=0.04,aspect='auto')
        ax[0,0].set_ylabel('Cell number')
        ax[0,0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0,0].set_xticks(xticks)
        ax[0,0].set_xticklabels(xticklabel)
        ax[0,0].invert_yaxis()
        ax[0,0].margins(x=0, y=0)


        ax[0,1].plot(np.array(areas_not_boundary)[sortorder],range(len(areas_not_boundary)),'-', c=cat_colors[catlist[c]])
        # ax[1,1].set_yticks([])
        ax[0,1].set_ylim(0,len(areas_not_boundary)-1)
        ax[0,1].set_xlim(0,max_area*1.05)
        ax[0,1].set_ylabel('Cell number')
        ax[0,1].set_xlabel('Area (µm$^2$)')

        # comment the following line out if you don't want the overlay of all single-cell diffusion spectra
        [ax[1,0].semilogx(D,j,color=cat_colors[catlist[c]], alpha=0.5,linewidth=0.5) for j in mpo/mpo.sum(1)[:,np.newaxis]];
        ax[1,0].semilogx(D,mpo_not_boundary.sum(0)/mpo_not_boundary.sum(0).sum(),color=cat_colors[catlist[c]],linewidth=2)
        ax[1,0].set_xticks([.01,.1,1,10,100])
        ax[1,0].set_xticklabels(['0.01','0.1','1','10','100'])
        ax[1,0].set_ylim([0,0.06])
        ax[1,0].set_xlim([0.01,100])
        ax[1,0].plot([boundthresh,boundthresh],ax[1,0].get_ylim(),'--',color=[.5,.5,.5])
        ax[1,0].set_xlabel('$D$ (µm$^2$/s)')
        ax[1,0].set_ylabel('Fraction of molecules')

        ax[1,1].hist(areas_not_boundary,color=cat_colors[catlist[c]],bins=np.linspace(0, max_area, 25))
        ax[1,1].set_xlim(ax[0,1].get_xlim())
        ax[1,1].set_xlabel('Area (µm$^2$)')
        ax[1,1].set_ylabel('Number of cells')

        fig.subplots_adjust(wspace=.4, hspace=.25)

        fig.savefig(outf+'/combo_plot_ranked_area_nonboundarycells_'+cat_names[catlist[c]]+'.png',dpi=300)
        fig.savefig(outf+'/combo_plot_ranked_area_nonboundarycells_'+cat_names[catlist[c]]+'.pdf',dpi=300)
        plt.show()

def plot_D_heatmap_ranked_intensity_raw(SAD, cellstats, cat_names=None, cat_colors=None, default_channel_name = 'main channel', outf='Figures', boundthresh = 0.15):
    '''
    For each cell category, generates a heatmap showing the distribution of inferred diffusion coefficients for each cell. Rank cells in the
    heatmap according to their raw intensities. Also plots the raw intensity of each cell, the PDFs, and the distribution of raw intensities.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        default_channel_name: name of the channel used to generate cell masks
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    max_intensity = np.max([np.max(cellstats[i]['intensity_mask'].to_numpy()) for i in list(cellstats.keys())])

    for c in range(len(catlist)):

        # sort rows by intensity
        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
        D = SAD[catlist[c]].likelihood.diff_coefs
        intensities = cellstats[catlist[c]]['intensity_mask'].to_numpy()
        sortorder = np.argsort(intensities)

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        # fig,ax = plt.subplots(nrows=2,ncols=2,sharey='row',sharex='col',figsize=(6,8))
        fig,ax = plt.subplots(nrows=2,ncols=2,sharey=False,sharex=False,figsize=(9,6))
        plt.suptitle(cat_names[catlist[c]])
        ax[0,0].imshow(mpoNorm[sortorder,:],vmin=0,vmax=0.04,aspect='auto')
        ax[0,0].set_ylabel('Cell number')
        ax[0,0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0,0].set_xticks(xticks)
        ax[0,0].set_xticklabels(xticklabel)
        ax[0,0].invert_yaxis()
        ax[0,0].margins(x=0, y=0)


        ax[0,1].plot(np.array(intensities)[sortorder],range(len(intensities)),'-', c=cat_colors[catlist[c]])
        # ax[1,1].set_yticks([])
        ax[0,1].set_ylim(0,len(intensities)-1)
        ax[0,1].set_xlim(0,max_intensity*1.05)
        ax[0,1].set_ylabel('Cell number')
        ax[0,1].set_xlabel(f'Mean {default_channel_name} intensity (AU)')

        # comment the following line out if you don't want the overlay of all single-cell diffusion spectra
        [ax[1,0].semilogx(D,j,color=cat_colors[catlist[c]], alpha=0.5,linewidth=0.5) for j in mpo/mpo.sum(1)[:,np.newaxis]];
        ax[1,0].semilogx(D,mpo.sum(0)/mpo.sum(0).sum(),color=cat_colors[catlist[c]],linewidth=2)
        ax[1,0].set_xticks([.01,.1,1,10,100])
        ax[1,0].set_xticklabels(['0.01','0.1','1','10','100'])
        ax[1,0].set_ylim([0,0.06])
        ax[1,0].set_xlim([0.01,100])
        ax[1,0].plot([boundthresh,boundthresh],ax[1,0].get_ylim(),'--',color=[.5,.5,.5])
        ax[1,0].set_xlabel('$D$ (µm$^2$/s)')
        ax[1,0].set_ylabel('Fraction of molecules')

        ax[1,1].hist(intensities,color=cat_colors[catlist[c]],bins=np.linspace(0, max_intensity, 25))
        ax[1,1].set_xlim(ax[0,1].get_xlim())
        ax[1,1].set_xlabel(f'Mean {default_channel_name} intensity (AU)')
        ax[1,1].set_ylabel('Number of cells')

        fig.subplots_adjust(wspace=.4, hspace=.25)

        fig.savefig(outf+'/combo_plot_ranked_int_raw_'+cat_names[catlist[c]]+'.png',dpi=300)
        fig.savefig(outf+'/combo_plot_ranked_int_raw_'+cat_names[catlist[c]]+'.pdf',dpi=300)
        plt.show()

def plot_D_heatmap_ranked_intensity_minusbg(SAD, cellstats, cat_names=None, cat_colors=None, default_channel_name='main channel', outf='Figures', boundthresh = 0.15):
    '''
    For each cell category, generates a heatmap showing the distribution of inferred diffusion coefficients for each cell. Rank cells in the
    heatmap according to their background-subtracted mean intensities. Also plots a scatter plot of the intensity of each cell against its
    fraction of bound trajectories, the PDFs, and the distribution of intensities.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        default_channel_name: name of the channel used to generate cell masks
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    max_intensity = np.max([np.max(cellstats[i]['intensity_mean'].to_numpy() - cellstats[i]['background'].to_numpy()) for i in list(cellstats.keys())])
    max_fbound = np.max([np.max(cellstats[i]['fbound'].to_numpy()) for i in list(cellstats.keys())])

    for c in range(len(catlist)):

        # sort rows by bound fraction
        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
        D = SAD[catlist[c]].likelihood.diff_coefs
        sel = (D<=boundthresh)
        fbound = mpoNorm[:,sel].sum(1)
        int_bsubt = cellstats[catlist[c]]['intensity_mean'].to_numpy() - cellstats[catlist[c]]['background'].to_numpy()
        sortorder = np.argsort(int_bsubt)

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        # fig,ax = plt.subplots(nrows=2,ncols=2,sharey='row',sharex='col',figsize=(6,8))
        fig,ax = plt.subplots(nrows=2,ncols=2,sharey=False,sharex=False,figsize=(9,6))
        plt.suptitle(cat_names[catlist[c]])
        ax[0,0].imshow(mpoNorm[sortorder,:],vmin=0,vmax=0.04,aspect='auto')
        ax[0,0].set_ylabel('Cell number')
        ax[0,0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0,0].set_xticks(xticks)
        ax[0,0].set_xticklabels(xticklabel)
        ax[0,0].invert_yaxis()
        ax[0,0].margins(x=0, y=0)


        ax[0,1].plot(int_bsubt[sortorder],fbound[sortorder],'.', c=cat_colors[catlist[c]])
        # ax[1,1].set_yticks([])
        ax[0,1].set_ylim(0,max_fbound*1.05)
        ax[0,1].set_xlim(0,max_intensity*1.05)
        ax[0,1].set_xlabel(f'Mean {default_channel_name} intensity (AU)')
        ax[0,1].set_ylabel('Fraction bound')

        # comment the following line out if you don't want the overlay of all single-cell diffusion spectra
        [ax[1,0].semilogx(D,j,color=cat_colors[catlist[c]], alpha=0.5,linewidth=0.5) for j in mpo/mpo.sum(1)[:,np.newaxis]];
        ax[1,0].semilogx(D,mpo.sum(0)/mpo.sum(0).sum(),color=cat_colors[catlist[c]],linewidth=2)
        ax[1,0].set_xticks([.01,.1,1,10,100])
        ax[1,0].set_xticklabels(['0.01','0.1','1','10','100'])
        ax[1,0].set_ylim([0,0.06])
        ax[1,0].set_xlim([0.01,100])
        ax[1,0].plot([boundthresh,boundthresh],ax[1,0].get_ylim(),'--',color=[.5,.5,.5])
        ax[1,0].set_xlabel('$D$ (µm$^2$/s)')
        ax[1,0].set_ylabel('Fraction of molecules')

        ax[1,1].hist(int_bsubt,color=cat_colors[catlist[c]],bins=np.linspace(0, max_intensity, 25))
        # ax[1,1].set_xlim(0,1)
        ax[1,1].set_xlabel(f'Mean {default_channel_name} intensity (AU)')
        ax[1,1].set_ylabel('Number of cells')
        ax[1,1].set_xlim(ax[0,1].get_xlim())

        fig.subplots_adjust(wspace=.4, hspace=.25)

        fig.savefig(outf+'/combo_plot_ranked_int_minusbg_'+cat_names[catlist[c]]+'.png',dpi=300)
        fig.savefig(outf+'/combo_plot_ranked_int_minusbg_'+cat_names[catlist[c]]+'.pdf',dpi=300)
        plt.show()

def plot_D_heatmap_ranked_eccentricity(SAD, cellstats, cat_names=None, cat_colors=None, outf='Figures', boundthresh = 0.15):
    '''
    For each cell category, generates a heatmap showing the distribution of inferred diffusion coefficients for each cell. Rank cells in the
    heatmap according to their eccentricities. Also plots a scatter plot of the eccentricity of each cell against its fraction of bound
    trajectories, the PDFs, and the distribution of eccentricities.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    max_fbound = np.max([np.max(cellstats[i]['fbound'].to_numpy()) for i in list(cellstats.keys())])

    for c in range(len(catlist)):

        # sort rows by bound fraction
        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
        D = SAD[catlist[c]].likelihood.diff_coefs
        sel = (D<=boundthresh)
        fbound = mpoNorm[:,sel].sum(1)
        ecc = cellstats[catlist[c]]['eccentricity'].to_numpy()
        not_on_boundary = list(cellstats[catlist[c]]['not_on_boundary'])

        # include only those that are not on the boundary
        fbound = fbound[not_on_boundary]
        ecc = ecc[not_on_boundary]
        mpoNorm = mpoNorm[not_on_boundary,:]

        sortorder = np.argsort(ecc)

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        # fig,ax = plt.subplots(nrows=2,ncols=2,sharey='row',sharex='col',figsize=(6,8))
        fig,ax = plt.subplots(nrows=2,ncols=2,sharey=False,sharex=False,figsize=(9,6))
        plt.suptitle(cat_names[catlist[c]])
        ax[0,0].imshow(mpoNorm[sortorder,:],vmin=0,vmax=0.04,aspect='auto')
        ax[0,0].set_ylabel('Eccentricity rank')
        ax[0,0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0,0].set_xticks(xticks)
        ax[0,0].set_xticklabels(xticklabel)
        ax[0,0].invert_yaxis()
        ax[0,0].margins(x=0, y=0)


        ax[0,1].plot(ecc[sortorder],fbound[sortorder],'.', c=cat_colors[catlist[c]])
        # ax[1,1].set_yticks([])
        ax[0,1].set_ylim(0,max_fbound*1.05)
        ax[0,1].set_xlim(0,1)
        ax[0,1].set_xlabel('Eccentricity')
        ax[0,1].set_ylabel('Fraction bound')

        # comment the following line out if you don't want the overlay of all single-cell diffusion spectra
        [ax[1,0].semilogx(D,j,color=cat_colors[catlist[c]], alpha=0.5,linewidth=0.5) for j in mpo/mpo.sum(1)[:,np.newaxis]];
        ax[1,0].semilogx(D,mpo.sum(0)/mpo.sum(0).sum(),color=cat_colors[catlist[c]],linewidth=2)
        ax[1,0].set_xticks([.01,.1,1,10,100])
        ax[1,0].set_xticklabels(['0.01','0.1','1','10','100'])
        ax[1,0].set_ylim([0,0.06])
        ax[1,0].set_xlim([0.01,100])
        ax[1,0].plot([boundthresh,boundthresh],ax[1,0].get_ylim(),'--',color=[.5,.5,.5])
        ax[1,0].set_xlabel('$D$ (µm$^2$/s)')
        ax[1,0].set_ylabel('Fraction of molecules')

        ax[1,1].hist(ecc,color=cat_colors[catlist[c]],bins=np.linspace(0, 1, 25))
        # ax[1,1].set_xlim(0,1)
        ax[1,1].set_xlabel('Eccentricity')
        ax[1,1].set_ylabel('Number of cells')
        ax[1,1].set_xlim(ax[0,1].get_xlim())

        fig.subplots_adjust(wspace=.4, hspace=.25)

        fig.savefig(outf+'/combo_plot_ranked_eccentricity_'+cat_names[catlist[c]]+'.png',dpi=300)
        fig.savefig(outf+'/combo_plot_ranked_eccentricity_'+cat_names[catlist[c]]+'.pdf',dpi=300)
        plt.show()

def plot_D_heatmap_ranked_locdensity(SAD, cellstats, cat_names=None, cat_colors=None, outf='Figures', boundthresh = 0.15):
    '''
    For each cell category, generates a heatmap showing the distribution of inferred diffusion coefficients for each cell. Rank cells in the
    heatmap according to the number of localizations normalized by the cell area. Also plots a scatter plot of the normalized number of
    localizations in each cell against its fraction of bound trajectories, the PDFs, and the distribution of the normalized number of localizations.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    max_fbound = np.max([np.max(cellstats[i]['fbound'].to_numpy()) for i in list(cellstats.keys())])

    for c in range(len(catlist)):

        # sort rows by bound fraction
        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
        D = SAD[catlist[c]].likelihood.diff_coefs
        sel = (D<=boundthresh)
        fbound = mpoNorm[:,sel].sum(1)
        numlocs = SAD[catlist[c]].processed_track_statistics['mean_detections_per_frame'].to_numpy()
        # areas = cellstats[catlist[c]]['area_mask'].to_numpy()
        # numlocs_per_area = numlocs/areas

        sortorder = np.argsort(numlocs)

        max_val = np.max([np.max(SAD[x].processed_track_statistics['mean_detections_per_frame'].to_numpy()) for x in catlist])

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        # fig,ax = plt.subplots(nrows=2,ncols=2,sharey='row',sharex='col',figsize=(6,8))
        fig,ax = plt.subplots(nrows=2,ncols=2,sharey=False,sharex=False,figsize=(9,6))
        plt.suptitle(cat_names[catlist[c]])
        ax[0,0].imshow(mpoNorm[sortorder,:],vmin=0,vmax=0.04,aspect='auto')
        ax[0,0].set_ylabel('Localization density rank')
        ax[0,0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0,0].set_xticks(xticks)
        ax[0,0].set_xticklabels(xticklabel)
        ax[0,0].invert_yaxis()
        ax[0,0].margins(x=0, y=0)


        ax[0,1].plot(numlocs[sortorder],fbound[sortorder],'.', c=cat_colors[catlist[c]])
        # ax[1,1].set_yticks([])
        ax[0,1].set_ylim(0,max_fbound*1.05)
        ax[0,1].set_xlim(0,max_val*1.05)
        ax[0,1].set_xlabel('# localizations per frame')
        ax[0,1].set_ylabel('Fraction bound')

        # comment the following line out if you don't want the overlay of all single-cell diffusion spectra
        [ax[1,0].semilogx(D,j,color=cat_colors[catlist[c]], alpha=0.5,linewidth=0.5) for j in mpo/mpo.sum(1)[:,np.newaxis]];
        ax[1,0].semilogx(D,mpo.sum(0)/mpo.sum(0).sum(),color=cat_colors[catlist[c]],linewidth=2)
        ax[1,0].set_xticks([.01,.1,1,10,100])
        ax[1,0].set_xticklabels(['0.01','0.1','1','10','100'])
        ax[1,0].set_ylim([0,0.06])
        ax[1,0].set_xlim([0.01,100])
        ax[1,0].plot([boundthresh,boundthresh],ax[1,0].get_ylim(),'--',color=[.5,.5,.5])
        ax[1,0].set_xlabel('$D$ (µm$^2$/s)')
        ax[1,0].set_ylabel('Fraction of molecules')

        ax[1,1].hist(numlocs,color=cat_colors[catlist[c]],bins=np.linspace(0, max_val, 25))
        # ax[1,1].set_xlim(0,1)
        ax[1,1].set_xlabel('# localizations per frame')
        ax[1,1].set_ylabel('Number of cells')
        ax[1,1].set_xlim(ax[0,1].get_xlim())

        fig.subplots_adjust(wspace=.4, hspace=.25)

        fig.savefig(outf+'/combo_plot_ranked_locdensity_'+cat_names[catlist[c]]+'.png',dpi=300)
        fig.savefig(outf+'/combo_plot_ranked_locdensity_'+cat_names[catlist[c]]+'.pdf',dpi=300)
        plt.show()

def plot_D_heatmap_ranked_cellcycle_ncratio(SAD, cellstats, cat_names=None, cat_colors=None, outf='Figures', boundthresh = 0.15):
    '''
    For each cell category, generates a heatmap showing the distribution of inferred diffusion coefficients for each cell. Rank cells in the
    heatmap according to their DHB nuclear/cytoplasmic ratios. Also plots a scatter plot of the DHB N/C ratio in each cell against its fraction of bound
    trajectories, the PDFs, and the distribution of N/C ratios.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    if 'cellcycle_ncratio' in cellstats[catlist[0]].columns:
        pass
    else:
        print('No cell cycle marker channel found.')
        return

    max_fbound = np.max([np.max(cellstats[i]['fbound'].to_numpy()) for i in list(cellstats.keys())])
    max_val = np.max([np.max(cellstats[i]['cellcycle_ncratio'].to_numpy()) for i in list(cellstats.keys())])

    for c in range(len(catlist)):

        # sort rows by bound fraction
        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
        D = SAD[catlist[c]].likelihood.diff_coefs
        sel = (D<=boundthresh)
        fbound = mpoNorm[:,sel].sum(1)
        cellcycle_ncratio = cellstats[catlist[c]]['cellcycle_ncratio'].to_numpy()
        not_on_boundary = list(cellstats[catlist[c]]['not_on_boundary'])

        # include only those that are not on the boundary
        # fbound = fbound[not_on_boundary]
        # cellcycle_ncratio = cellcycle_ncratio[not_on_boundary]
        # mpoNorm = mpoNorm[not_on_boundary,:]

        sortorder = np.argsort(cellcycle_ncratio)

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        # fig,ax = plt.subplots(nrows=2,ncols=2,sharey='row',sharex='col',figsize=(6,8))
        fig,ax = plt.subplots(nrows=2,ncols=2,sharey=False,sharex=False,figsize=(9,6))
        plt.suptitle(cat_names[catlist[c]])
        ax[0,0].imshow(mpoNorm[sortorder,:],vmin=0,vmax=0.04,aspect='auto')
        ax[0,0].set_ylabel('DHB N/C ratio rank')
        ax[0,0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0,0].set_xticks(xticks)
        ax[0,0].set_xticklabels(xticklabel)
        ax[0,0].invert_yaxis()
        ax[0,0].margins(x=0, y=0)


        ax[0,1].plot(cellcycle_ncratio[sortorder],fbound[sortorder],'.', c=cat_colors[catlist[c]])
        # ax[1,1].set_yticks([])
        ax[0,1].set_ylim(0,max_fbound*1.05)
        ax[0,1].set_xlim(0,max_val*1.05)
        ax[0,1].set_xlabel('DHB N/C ratio')
        ax[0,1].set_ylabel('Fraction bound')

        # comment the following line out if you don't want the overlay of all single-cell diffusion spectra
        [ax[1,0].semilogx(D,j,color=cat_colors[catlist[c]], alpha=0.5,linewidth=0.5) for j in mpo/mpo.sum(1)[:,np.newaxis]];
        ax[1,0].semilogx(D,mpo.sum(0)/mpo.sum(0).sum(),color=cat_colors[catlist[c]],linewidth=2)
        ax[1,0].set_xticks([.01,.1,1,10,100])
        ax[1,0].set_xticklabels(['0.01','0.1','1','10','100'])
        ax[1,0].set_ylim([0,0.06])
        ax[1,0].set_xlim([0.01,100])
        ax[1,0].plot([boundthresh,boundthresh],ax[1,0].get_ylim(),'--',color=[.5,.5,.5])
        ax[1,0].set_xlabel('$D$ (µm$^2$/s)')
        ax[1,0].set_ylabel('Fraction of molecules')

        ax[1,1].hist(cellcycle_ncratio,color=cat_colors[catlist[c]],bins=np.linspace(0, max_val, 25))
        # ax[1,1].set_xlim(0,1)
        ax[1,1].set_xlabel('DHB N/C ratio')
        ax[1,1].set_ylabel('Number of cells')
        ax[1,1].set_xlim(ax[0,1].get_xlim())

        fig.subplots_adjust(wspace=.4, hspace=.25)

        fig.savefig(outf+'/combo_plot_ranked_cellcycle_ncratio_'+cat_names[catlist[c]]+'.png',dpi=300)
        fig.savefig(outf+'/combo_plot_ranked_cellcycle_ncratio_'+cat_names[catlist[c]]+'.pdf',dpi=300)
        plt.show()

def plot_D_heatmap_ranked_fbound_allcats(SAD, cellstats, cat_names=None, cat_colors=None, outf='Figures', boundthresh = 0.15, localsort=False, sep_cat_columns=True):
    '''
    For each cell category, generates a heatmap showing the distribution of inferred diffusion coefficients for each cell. Rank cells in the
    heatmap according to the fraction of bound trajectories.
    Sorting can be performed within each category (localsort=True) or across all categories (localsort=False).
    If sorting is performed across categories, the category that the cell in each row of the heatmap belongs to can be visualized in either a
    single colorbar (sep_cat_columns=False) or in separate colorbars (sep_cat_columns=True).

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
        localsort: boolean indicating whether rows (cells) are to be sorted within each category (True) or globally (False)
        sep_cat_columns: boolean indicating whether the category that each cell belongs to is to be visualized in a single colorbar (False) or in separate colorbars (True)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    for c in range(len(catlist)):

        # sort rows by bound fraction
        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
        D = SAD[catlist[c]].likelihood.diff_coefs
        sel = (D<=boundthresh)
        fbound = mpoNorm[:,sel].sum(1)
        sortorder = np.argsort(fbound)
        mpoNorm_sorted_locally_c = mpoNorm[sortorder,:]
        mpoNorm_sorted_locally_ids_c = np.zeros((mpoNorm.shape[0],1)) + catlist[c]
        if c == 0:
            mpoNorm_sorted_locally = mpoNorm_sorted_locally_c
            mpoNorm_sorted_locally_ids = mpoNorm_sorted_locally_ids_c
            sortparam_sorted_locally = fbound[sortorder]
        else:
            mpoNorm_sorted_locally = np.append(mpoNorm_sorted_locally, mpoNorm_sorted_locally_c, axis=0)
            mpoNorm_sorted_locally_ids = np.append(mpoNorm_sorted_locally_ids, mpoNorm_sorted_locally_ids_c, axis=0)
            sortparam_sorted_locally = np.append(sortparam_sorted_locally, fbound[sortorder])

    if localsort:

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(9,6))

        ax[0].imshow(mpoNorm_sorted_locally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        cat_cmap = mcolors.ListedColormap(list(cat_colors.values()))
        ax[1].imshow(mpoNorm_sorted_locally_ids, cmap=cat_cmap, aspect=0.5)
        ax[1].invert_yaxis()
        ax[1].get_xaxis().set_visible(False)
        ax[1].get_yaxis().set_visible(False)
        ax[1].yaxis.tick_right()
        ax[1].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
        ax[1].margins(x=0, y=0)
        ax[1].spines['bottom'].set_color('lightgray')
        ax[1].spines['top'].set_color('lightgray')
        ax[1].spines['right'].set_color('lightgray')
        ax[1].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=1.2)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_fbound_allcats_localsort.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_fbound_allcats_localsort.pdf',dpi=300)
        plt.show()

    elif not sep_cat_columns:

        sortorder_global = np.argsort(sortparam_sorted_locally)
        mpoNorm_sorted_globally = mpoNorm_sorted_locally[sortorder_global,:]
        mpoNorm_sorted_globally_ids = mpoNorm_sorted_locally_ids[sortorder_global,:]

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(9,6))

        ax[0].imshow(mpoNorm_sorted_globally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        cat_cmap = mcolors.ListedColormap(list(cat_colors.values()))
        ax[1].imshow(mpoNorm_sorted_globally_ids, cmap=cat_cmap, aspect=0.5)
        ax[1].invert_yaxis()
        ax[1].get_xaxis().set_visible(False)
        ax[1].get_yaxis().set_visible(False)
        ax[1].yaxis.tick_right()
        ax[1].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
        ax[1].margins(x=0, y=0)
        ax[1].spines['bottom'].set_color('lightgray')
        ax[1].spines['top'].set_color('lightgray')
        ax[1].spines['right'].set_color('lightgray')
        ax[1].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=1.2)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_fbound_allcats_globalsort.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_fbound_allcats_globalsort.pdf',dpi=300)
        plt.show()

    elif sep_cat_columns:

        sortorder_global = np.argsort(sortparam_sorted_locally)
        mpoNorm_sorted_globally = mpoNorm_sorted_locally[sortorder_global,:]
        mpoNorm_sorted_globally_ids = mpoNorm_sorted_locally_ids[sortorder_global,:]

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        wratios = np.append([30],[1 for i in catlist])
        fig,ax = plt.subplots(nrows=1,ncols=len(catlist)+1,sharey=False,sharex=False, figsize=(9,6), gridspec_kw={'width_ratios':wratios})

        ax[0].imshow(mpoNorm_sorted_globally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        for c in catlist:
            cat_cmap_new_list = np.append(['w'],[cat_colors[c]])
            cat_cmap = mcolors.ListedColormap(cat_cmap_new_list)
            mpoNorm_sorted_globally_ids_c = np.array([1 if x==c else 0 for x in mpoNorm_sorted_globally_ids])
            mpoNorm_sorted_globally_ids_c = np.expand_dims(mpoNorm_sorted_globally_ids_c, axis=1)
            ax[c].imshow(mpoNorm_sorted_globally_ids_c, cmap=cat_cmap, aspect=0.5)
            ax[c].invert_yaxis()
            ax[c].get_xaxis().set_visible(False)
            ax[c].get_yaxis().set_visible(False)
            ax[c].yaxis.tick_right()
            ax[c].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
            ax[c].margins(x=0, y=0)
            # ax[c].axis('off')
            ax[c].spines['bottom'].set_color('lightgray')
            ax[c].spines['top'].set_color('lightgray')
            ax[c].spines['right'].set_color('lightgray')
            ax[c].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=0.4)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_fbound_allcats_globalsort_sepcols.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_fbound_allcats_globalsort_sepcols.pdf',dpi=300)
        plt.show()

def plot_D_heatmap_ranked_area_allcells_allcats(SAD, cellstats, cat_names=None, cat_colors=None, outf='Figures', boundthresh = 0.15, localsort=False, sep_cat_columns=True):
    '''
    For each cell category, generates a heatmap showing the distribution of inferred diffusion coefficients for each cell. Rank cells in the
    heatmap according to their areas.
    Sorting can be performed within each category (localsort=True) or across all categories (localsort=False).
    If sorting is performed across categories, the category that the cell in each row of the heatmap belongs to can be visualized in either a
    single colorbar (sep_cat_columns=False) or in separate colorbars (sep_cat_columns=True).

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
        localsort: boolean indicating whether rows (cells) are to be sorted within each category (True) or globally (False)
        sep_cat_columns: boolean indicating whether the category that each cell belongs to is to be visualized in a single colorbar (False) or in separate colorbars (True)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    for c in range(len(catlist)):

        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
        D = SAD[catlist[c]].likelihood.diff_coefs
        areas = cellstats[catlist[c]]['area_mask'].to_numpy()
        sortorder = np.argsort(areas)
        mpoNorm_sorted_locally_c = mpoNorm[sortorder,:]
        mpoNorm_sorted_locally_ids_c = np.zeros((mpoNorm.shape[0],1)) + catlist[c]
        if c == 0:
            mpoNorm_sorted_locally = mpoNorm_sorted_locally_c
            mpoNorm_sorted_locally_ids = mpoNorm_sorted_locally_ids_c
            sortparam_sorted_locally = areas[sortorder]
        else:
            mpoNorm_sorted_locally = np.append(mpoNorm_sorted_locally, mpoNorm_sorted_locally_c, axis=0)
            mpoNorm_sorted_locally_ids = np.append(mpoNorm_sorted_locally_ids, mpoNorm_sorted_locally_ids_c, axis=0)
            sortparam_sorted_locally = np.append(sortparam_sorted_locally, areas[sortorder])

    if localsort:

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(9,6))

        ax[0].imshow(mpoNorm_sorted_locally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        cat_cmap = mcolors.ListedColormap(list(cat_colors.values()))
        ax[1].imshow(mpoNorm_sorted_locally_ids, cmap=cat_cmap, aspect=0.5)
        ax[1].invert_yaxis()
        ax[1].get_xaxis().set_visible(False)
        ax[1].get_yaxis().set_visible(False)
        ax[1].yaxis.tick_right()
        ax[1].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
        ax[1].margins(x=0, y=0)
        ax[1].spines['bottom'].set_color('lightgray')
        ax[1].spines['top'].set_color('lightgray')
        ax[1].spines['right'].set_color('lightgray')
        ax[1].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=1.2)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_area_allcells_allcats_localsort.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_area_allcells_allcats_localsort.pdf',dpi=300)
        plt.show()

    elif not sep_cat_columns:

        sortorder_global = np.argsort(sortparam_sorted_locally)
        mpoNorm_sorted_globally = mpoNorm_sorted_locally[sortorder_global,:]
        mpoNorm_sorted_globally_ids = mpoNorm_sorted_locally_ids[sortorder_global,:]

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(9,6))

        ax[0].imshow(mpoNorm_sorted_globally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        cat_cmap = mcolors.ListedColormap(list(cat_colors.values()))
        ax[1].imshow(mpoNorm_sorted_globally_ids, cmap=cat_cmap, aspect=0.5)
        ax[1].invert_yaxis()
        ax[1].get_xaxis().set_visible(False)
        ax[1].get_yaxis().set_visible(False)
        ax[1].yaxis.tick_right()
        ax[1].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
        ax[1].margins(x=0, y=0)
        ax[1].spines['bottom'].set_color('lightgray')
        ax[1].spines['top'].set_color('lightgray')
        ax[1].spines['right'].set_color('lightgray')
        ax[1].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=1.2)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_area_allcells_allcats_globalsort.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_area_allcells_allcats_globalsort.pdf',dpi=300)
        plt.show()

    elif sep_cat_columns:

        sortorder_global = np.argsort(sortparam_sorted_locally)
        mpoNorm_sorted_globally = mpoNorm_sorted_locally[sortorder_global,:]
        mpoNorm_sorted_globally_ids = mpoNorm_sorted_locally_ids[sortorder_global,:]

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        wratios = np.append([30],[1 for i in catlist])
        fig,ax = plt.subplots(nrows=1,ncols=len(catlist)+1,sharey=False,sharex=False, figsize=(9,6), gridspec_kw={'width_ratios':wratios})

        ax[0].imshow(mpoNorm_sorted_globally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        for c in catlist:
            cat_cmap_new_list = np.append(['w'],[cat_colors[c]])
            cat_cmap = mcolors.ListedColormap(cat_cmap_new_list)
            mpoNorm_sorted_globally_ids_c = np.array([1 if x==c else 0 for x in mpoNorm_sorted_globally_ids])
            mpoNorm_sorted_globally_ids_c = np.expand_dims(mpoNorm_sorted_globally_ids_c, axis=1)
            ax[c].imshow(mpoNorm_sorted_globally_ids_c, cmap=cat_cmap, aspect=0.5)
            ax[c].invert_yaxis()
            ax[c].get_xaxis().set_visible(False)
            ax[c].get_yaxis().set_visible(False)
            ax[c].yaxis.tick_right()
            ax[c].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
            ax[c].margins(x=0, y=0)
            # ax[c].axis('off')
            ax[c].spines['bottom'].set_color('lightgray')
            ax[c].spines['top'].set_color('lightgray')
            ax[c].spines['right'].set_color('lightgray')
            ax[c].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=0.4)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_area_allcells_allcats_globalsort_sepcols.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_area_allcells_allcats_globalsort_sepcols.pdf',dpi=300)
        plt.show()

def plot_D_heatmap_ranked_area_nonboundarycells_allcats(SAD, cellstats, cat_names=None, cat_colors=None, outf='Figures', boundthresh = 0.15, localsort=False, sep_cat_columns=True):
    '''
    For each cell category, generates a heatmap showing the distribution of inferred diffusion coefficients for each cell. Rank cells in the
    heatmap according to their areas considering only cells that are not at the boundary of the ROI.
    Sorting can be performed within each category (localsort=True) or across all categories (localsort=False).
    If sorting is performed across categories, the category that the cell in each row of the heatmap belongs to can be visualized in either a
    single colorbar (sep_cat_columns=False) or in separate colorbars (sep_cat_columns=True).

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
        localsort: boolean indicating whether rows (cells) are to be sorted within each category (True) or globally (False)
        sep_cat_columns: boolean indicating whether the category that each cell belongs to is to be visualized in a single colorbar (False) or in separate colorbars (True)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    for c in range(len(catlist)):

        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
        D = SAD[catlist[c]].likelihood.diff_coefs
        areas = cellstats[catlist[c]]['area_mask'].to_numpy()
        not_on_boundary = list(cellstats[catlist[c]]['not_on_boundary'])
        areas_not_boundary = np.asarray(areas)[not_on_boundary]
        mpo_not_boundary = mpo[not_on_boundary]
        mpoNorm_not_boundary = mpoNorm[not_on_boundary]
        sortorder = np.argsort(areas_not_boundary)
        mpoNorm_sorted_locally_c = mpoNorm_not_boundary[sortorder,:]
        mpoNorm_sorted_locally_ids_c = np.zeros((mpoNorm_not_boundary.shape[0],1)) + catlist[c]
        if c == 0:
            mpoNorm_sorted_locally = mpoNorm_sorted_locally_c
            mpoNorm_sorted_locally_ids = mpoNorm_sorted_locally_ids_c
            sortparam_sorted_locally = areas_not_boundary[sortorder]
        else:
            mpoNorm_sorted_locally = np.append(mpoNorm_sorted_locally, mpoNorm_sorted_locally_c, axis=0)
            mpoNorm_sorted_locally_ids = np.append(mpoNorm_sorted_locally_ids, mpoNorm_sorted_locally_ids_c, axis=0)
            sortparam_sorted_locally = np.append(sortparam_sorted_locally, areas_not_boundary[sortorder])

    if localsort:

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(9,6))

        ax[0].imshow(mpoNorm_sorted_locally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        cat_cmap = mcolors.ListedColormap(list(cat_colors.values()))
        ax[1].imshow(mpoNorm_sorted_locally_ids, cmap=cat_cmap, aspect=0.5)
        ax[1].invert_yaxis()
        ax[1].get_xaxis().set_visible(False)
        ax[1].get_yaxis().set_visible(False)
        ax[1].yaxis.tick_right()
        ax[1].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
        ax[1].margins(x=0, y=0)
        ax[1].spines['bottom'].set_color('lightgray')
        ax[1].spines['top'].set_color('lightgray')
        ax[1].spines['right'].set_color('lightgray')
        ax[1].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=1.2)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_area_nonboundarycells_allcats_localsort.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_area_nonboundarycells_allcats_localsort.pdf',dpi=300)
        plt.show()

    elif not sep_cat_columns:

        sortorder_global = np.argsort(sortparam_sorted_locally)
        mpoNorm_sorted_globally = mpoNorm_sorted_locally[sortorder_global,:]
        mpoNorm_sorted_globally_ids = mpoNorm_sorted_locally_ids[sortorder_global,:]

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(9,6))

        ax[0].imshow(mpoNorm_sorted_globally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        cat_cmap = mcolors.ListedColormap(list(cat_colors.values()))
        ax[1].imshow(mpoNorm_sorted_globally_ids, cmap=cat_cmap, aspect=0.5)
        ax[1].invert_yaxis()
        ax[1].get_xaxis().set_visible(False)
        ax[1].get_yaxis().set_visible(False)
        ax[1].yaxis.tick_right()
        ax[1].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
        ax[1].margins(x=0, y=0)
        ax[1].spines['bottom'].set_color('lightgray')
        ax[1].spines['top'].set_color('lightgray')
        ax[1].spines['right'].set_color('lightgray')
        ax[1].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=1.2)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_area_nonboundarycells_allcats_globalsort.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_area_nonboundaryells_allcats_globalsort.pdf',dpi=300)
        plt.show()

    elif sep_cat_columns:

        sortorder_global = np.argsort(sortparam_sorted_locally)
        mpoNorm_sorted_globally = mpoNorm_sorted_locally[sortorder_global,:]
        mpoNorm_sorted_globally_ids = mpoNorm_sorted_locally_ids[sortorder_global,:]

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        wratios = np.append([30],[1 for i in catlist])
        fig,ax = plt.subplots(nrows=1,ncols=len(catlist)+1,sharey=False,sharex=False, figsize=(9,6), gridspec_kw={'width_ratios':wratios})

        ax[0].imshow(mpoNorm_sorted_globally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        for c in catlist:
            cat_cmap_new_list = np.append(['w'],[cat_colors[c]])
            cat_cmap = mcolors.ListedColormap(cat_cmap_new_list)
            mpoNorm_sorted_globally_ids_c = np.array([1 if x==c else 0 for x in mpoNorm_sorted_globally_ids])
            mpoNorm_sorted_globally_ids_c = np.expand_dims(mpoNorm_sorted_globally_ids_c, axis=1)
            ax[c].imshow(mpoNorm_sorted_globally_ids_c, cmap=cat_cmap, aspect=0.5)
            ax[c].invert_yaxis()
            ax[c].get_xaxis().set_visible(False)
            ax[c].get_yaxis().set_visible(False)
            ax[c].yaxis.tick_right()
            ax[c].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
            ax[c].margins(x=0, y=0)
            # ax[c].axis('off')
            ax[c].spines['bottom'].set_color('lightgray')
            ax[c].spines['top'].set_color('lightgray')
            ax[c].spines['right'].set_color('lightgray')
            ax[c].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=0.4)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_area_nonboundarycells_allcats_globalsort_sepcols.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_area_nonboundarycells_allcats_globalsort_sepcols.pdf',dpi=300)
        plt.show()

def plot_D_heatmap_ranked_intensity_raw_allcats(SAD, cellstats, cat_names=None, cat_colors=None, outf='Figures', boundthresh = 0.15, localsort=False, sep_cat_columns=True):
    '''
    For each cell category, generates a heatmap showing the distribution of inferred diffusion coefficients for each cell. Rank cells in the
    heatmap according to their raw mean intensities.
    Sorting can be performed within each category (localsort=True) or across all categories (localsort=False).
    If sorting is performed across categories, the category that the cell in each row of the heatmap belongs to can be visualized in either a
    single colorbar (sep_cat_columns=False) or in separate colorbars (sep_cat_columns=True).

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
        localsort: boolean indicating whether rows (cells) are to be sorted within each category (True) or globally (False)
        sep_cat_columns: boolean indicating whether the category that each cell belongs to is to be visualized in a single colorbar (False) or in separate colorbars (True)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    for c in range(len(catlist)):

        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
        D = SAD[catlist[c]].likelihood.diff_coefs
        intensities = cellstats[catlist[c]]['intensity_mask'].to_numpy()
        sortorder = np.argsort(intensities)
        
        mpoNorm_sorted_locally_c = mpoNorm[sortorder,:]
        mpoNorm_sorted_locally_ids_c = np.zeros((mpoNorm.shape[0],1)) + catlist[c]
        if c == 0:
            mpoNorm_sorted_locally = mpoNorm_sorted_locally_c
            mpoNorm_sorted_locally_ids = mpoNorm_sorted_locally_ids_c
            sortparam_sorted_locally = intensities[sortorder]
        else:
            mpoNorm_sorted_locally = np.append(mpoNorm_sorted_locally, mpoNorm_sorted_locally_c, axis=0)
            mpoNorm_sorted_locally_ids = np.append(mpoNorm_sorted_locally_ids, mpoNorm_sorted_locally_ids_c, axis=0)
            sortparam_sorted_locally = np.append(sortparam_sorted_locally, intensities[sortorder])

    if localsort:

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(9,6))

        ax[0].imshow(mpoNorm_sorted_locally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        cat_cmap = mcolors.ListedColormap(list(cat_colors.values()))
        ax[1].imshow(mpoNorm_sorted_locally_ids, cmap=cat_cmap, aspect=0.5)
        ax[1].invert_yaxis()
        ax[1].get_xaxis().set_visible(False)
        ax[1].get_yaxis().set_visible(False)
        ax[1].yaxis.tick_right()
        ax[1].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
        ax[1].margins(x=0, y=0)
        ax[1].spines['bottom'].set_color('lightgray')
        ax[1].spines['top'].set_color('lightgray')
        ax[1].spines['right'].set_color('lightgray')
        ax[1].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=1.2)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_intensity_raw_allcats_localsort.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_intensity_raw_allcats_localsort.pdf',dpi=300)
        plt.show()

    elif not sep_cat_columns:

        sortorder_global = np.argsort(sortparam_sorted_locally)
        mpoNorm_sorted_globally = mpoNorm_sorted_locally[sortorder_global,:]
        mpoNorm_sorted_globally_ids = mpoNorm_sorted_locally_ids[sortorder_global,:]

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(9,6))

        ax[0].imshow(mpoNorm_sorted_globally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        cat_cmap = mcolors.ListedColormap(list(cat_colors.values()))
        ax[1].imshow(mpoNorm_sorted_globally_ids, cmap=cat_cmap, aspect=0.5)
        ax[1].invert_yaxis()
        ax[1].get_xaxis().set_visible(False)
        ax[1].get_yaxis().set_visible(False)
        ax[1].yaxis.tick_right()
        ax[1].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
        ax[1].margins(x=0, y=0)
        ax[1].spines['bottom'].set_color('lightgray')
        ax[1].spines['top'].set_color('lightgray')
        ax[1].spines['right'].set_color('lightgray')
        ax[1].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=1.2)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_intensity_raw_allcats_globalsort.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_intensity_raw_allcats_globalsort.pdf',dpi=300)
        plt.show()

    elif sep_cat_columns:

        sortorder_global = np.argsort(sortparam_sorted_locally)
        mpoNorm_sorted_globally = mpoNorm_sorted_locally[sortorder_global,:]
        mpoNorm_sorted_globally_ids = mpoNorm_sorted_locally_ids[sortorder_global,:]

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        wratios = np.append([30],[1 for i in catlist])
        fig,ax = plt.subplots(nrows=1,ncols=len(catlist)+1,sharey=False,sharex=False, figsize=(9,6), gridspec_kw={'width_ratios':wratios})

        ax[0].imshow(mpoNorm_sorted_globally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        for c in catlist:
            cat_cmap_new_list = np.append(['w'],[cat_colors[c]])
            cat_cmap = mcolors.ListedColormap(cat_cmap_new_list)
            mpoNorm_sorted_globally_ids_c = np.array([1 if x==c else 0 for x in mpoNorm_sorted_globally_ids])
            mpoNorm_sorted_globally_ids_c = np.expand_dims(mpoNorm_sorted_globally_ids_c, axis=1)
            ax[c].imshow(mpoNorm_sorted_globally_ids_c, cmap=cat_cmap, aspect=0.5)
            ax[c].invert_yaxis()
            ax[c].get_xaxis().set_visible(False)
            ax[c].get_yaxis().set_visible(False)
            ax[c].yaxis.tick_right()
            ax[c].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
            ax[c].margins(x=0, y=0)
            # ax[c].axis('off')
            ax[c].spines['bottom'].set_color('lightgray')
            ax[c].spines['top'].set_color('lightgray')
            ax[c].spines['right'].set_color('lightgray')
            ax[c].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=0.4)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_intensity_raw_allcats_globalsort_sepcols.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_intensity_raw_allcats_globalsort_sepcols.pdf',dpi=300)
        plt.show()

def plot_D_heatmap_ranked_intensity_minusbg_allcats(SAD, cellstats, cat_names=None, cat_colors=None, outf='Figures', boundthresh = 0.15, localsort=False, sep_cat_columns=True):
    '''
    For each cell category, generates a heatmap showing the distribution of inferred diffusion coefficients for each cell. Rank cells in the
    heatmap according to their background-subtracted mean intensities.
    Sorting can be performed within each category (localsort=True) or across all categories (localsort=False).
    If sorting is performed across categories, the category that the cell in each row of the heatmap belongs to can be visualized in either a
    single colorbar (sep_cat_columns=False) or in separate colorbars (sep_cat_columns=True).

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
        localsort: boolean indicating whether rows (cells) are to be sorted within each category (True) or globally (False)
        sep_cat_columns: boolean indicating whether the category that each cell belongs to is to be visualized in a single colorbar (False) or in separate colorbars (True)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    for c in range(len(catlist)):

        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
        D = SAD[catlist[c]].likelihood.diff_coefs
        sel = (D<=boundthresh)
        fbound = mpoNorm[:,sel].sum(1)
        int_bsubt = cellstats[catlist[c]]['intensity_mean'].to_numpy() - cellstats[catlist[c]]['background'].to_numpy()
        sortorder = np.argsort(int_bsubt)
        
        mpoNorm_sorted_locally_c = mpoNorm[sortorder,:]
        mpoNorm_sorted_locally_ids_c = np.zeros((mpoNorm.shape[0],1)) + catlist[c]
        if c == 0:
            mpoNorm_sorted_locally = mpoNorm_sorted_locally_c
            mpoNorm_sorted_locally_ids = mpoNorm_sorted_locally_ids_c
            sortparam_sorted_locally = int_bsubt[sortorder]
        else:
            mpoNorm_sorted_locally = np.append(mpoNorm_sorted_locally, mpoNorm_sorted_locally_c, axis=0)
            mpoNorm_sorted_locally_ids = np.append(mpoNorm_sorted_locally_ids, mpoNorm_sorted_locally_ids_c, axis=0)
            sortparam_sorted_locally = np.append(sortparam_sorted_locally, int_bsubt[sortorder])

    if localsort:

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(9,6))

        ax[0].imshow(mpoNorm_sorted_locally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        cat_cmap = mcolors.ListedColormap(list(cat_colors.values()))
        ax[1].imshow(mpoNorm_sorted_locally_ids, cmap=cat_cmap, aspect=0.5)
        ax[1].invert_yaxis()
        ax[1].get_xaxis().set_visible(False)
        ax[1].get_yaxis().set_visible(False)
        ax[1].yaxis.tick_right()
        ax[1].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
        ax[1].margins(x=0, y=0)
        ax[1].spines['bottom'].set_color('lightgray')
        ax[1].spines['top'].set_color('lightgray')
        ax[1].spines['right'].set_color('lightgray')
        ax[1].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=1.2)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_intensity_minusbg_allcats_localsort.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_intensity_minusbg_allcats_localsort.pdf',dpi=300)
        plt.show()

    elif not sep_cat_columns:

        sortorder_global = np.argsort(sortparam_sorted_locally)
        mpoNorm_sorted_globally = mpoNorm_sorted_locally[sortorder_global,:]
        mpoNorm_sorted_globally_ids = mpoNorm_sorted_locally_ids[sortorder_global,:]

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(9,6))

        ax[0].imshow(mpoNorm_sorted_globally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        cat_cmap = mcolors.ListedColormap(list(cat_colors.values()))
        ax[1].imshow(mpoNorm_sorted_globally_ids, cmap=cat_cmap, aspect=0.5)
        ax[1].invert_yaxis()
        ax[1].get_xaxis().set_visible(False)
        ax[1].get_yaxis().set_visible(False)
        ax[1].yaxis.tick_right()
        ax[1].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
        ax[1].margins(x=0, y=0)
        ax[1].spines['bottom'].set_color('lightgray')
        ax[1].spines['top'].set_color('lightgray')
        ax[1].spines['right'].set_color('lightgray')
        ax[1].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=1.2)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_intensity_minusbg_allcats_globalsort.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_intensity_minusbg_allcats_globalsort.pdf',dpi=300)
        plt.show()

    elif sep_cat_columns:

        sortorder_global = np.argsort(sortparam_sorted_locally)
        mpoNorm_sorted_globally = mpoNorm_sorted_locally[sortorder_global,:]
        mpoNorm_sorted_globally_ids = mpoNorm_sorted_locally_ids[sortorder_global,:]

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        wratios = np.append([30],[1 for i in catlist])
        fig,ax = plt.subplots(nrows=1,ncols=len(catlist)+1,sharey=False,sharex=False, figsize=(9,6), gridspec_kw={'width_ratios':wratios})

        ax[0].imshow(mpoNorm_sorted_globally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        for c in catlist:
            cat_cmap_new_list = np.append(['w'],[cat_colors[c]])
            cat_cmap = mcolors.ListedColormap(cat_cmap_new_list)
            mpoNorm_sorted_globally_ids_c = np.array([1 if x==c else 0 for x in mpoNorm_sorted_globally_ids])
            mpoNorm_sorted_globally_ids_c = np.expand_dims(mpoNorm_sorted_globally_ids_c, axis=1)
            ax[c].imshow(mpoNorm_sorted_globally_ids_c, cmap=cat_cmap, aspect=0.5)
            ax[c].invert_yaxis()
            ax[c].get_xaxis().set_visible(False)
            ax[c].get_yaxis().set_visible(False)
            ax[c].yaxis.tick_right()
            ax[c].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
            ax[c].margins(x=0, y=0)
            # ax[c].axis('off')
            ax[c].spines['bottom'].set_color('lightgray')
            ax[c].spines['top'].set_color('lightgray')
            ax[c].spines['right'].set_color('lightgray')
            ax[c].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=0.4)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_intensity_minusbg_allcats_globalsort_sepcols.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_intensity_minusbg_allcats_globalsort_sepcols.pdf',dpi=300)
        plt.show()

def plot_D_heatmap_ranked_eccentricity_allcats(SAD, cellstats, cat_names=None, cat_colors=None, outf='Figures', boundthresh = 0.15, localsort=False, sep_cat_columns=True):
    '''
    For each cell category, generates a heatmap showing the distribution of inferred diffusion coefficients for each cell. Rank cells in the
    heatmap according to their eccentricities, considering only cells that are not on the boundary of the ROI.
    Sorting can be performed within each category (localsort=True) or across all categories (localsort=False).
    If sorting is performed across categories, the category that the cell in each row of the heatmap belongs to can be visualized in either a
    single colorbar (sep_cat_columns=False) or in separate colorbars (sep_cat_columns=True).

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
        localsort: boolean indicating whether rows (cells) are to be sorted within each category (True) or globally (False)
        sep_cat_columns: boolean indicating whether the category that each cell belongs to is to be visualized in a single colorbar (False) or in separate colorbars (True)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    for c in range(len(catlist)):

        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
        D = SAD[catlist[c]].likelihood.diff_coefs
        sel = (D<=boundthresh)
        fbound = mpoNorm[:,sel].sum(1)
        ecc = cellstats[catlist[c]]['eccentricity'].to_numpy()
        not_on_boundary = list(cellstats[catlist[c]]['not_on_boundary'])

        # include only those that are not on the boundary
        fbound = fbound[not_on_boundary]
        ecc = ecc[not_on_boundary]
        mpoNorm = mpoNorm[not_on_boundary,:]

        sortorder = np.argsort(ecc)

        mpoNorm_sorted_locally_c = mpoNorm[sortorder,:]
        mpoNorm_sorted_locally_ids_c = np.zeros((mpoNorm.shape[0],1)) + catlist[c]
        if c == 0:
            mpoNorm_sorted_locally = mpoNorm_sorted_locally_c
            mpoNorm_sorted_locally_ids = mpoNorm_sorted_locally_ids_c
            sortparam_sorted_locally = ecc[sortorder]
        else:
            mpoNorm_sorted_locally = np.append(mpoNorm_sorted_locally, mpoNorm_sorted_locally_c, axis=0)
            mpoNorm_sorted_locally_ids = np.append(mpoNorm_sorted_locally_ids, mpoNorm_sorted_locally_ids_c, axis=0)
            sortparam_sorted_locally = np.append(sortparam_sorted_locally, ecc[sortorder])

    if localsort:

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(9,6))

        ax[0].imshow(mpoNorm_sorted_locally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        cat_cmap = mcolors.ListedColormap(list(cat_colors.values()))
        ax[1].imshow(mpoNorm_sorted_locally_ids, cmap=cat_cmap, aspect=0.5)
        ax[1].invert_yaxis()
        ax[1].get_xaxis().set_visible(False)
        ax[1].get_yaxis().set_visible(False)
        ax[1].yaxis.tick_right()
        ax[1].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
        ax[1].margins(x=0, y=0)
        ax[1].spines['bottom'].set_color('lightgray')
        ax[1].spines['top'].set_color('lightgray')
        ax[1].spines['right'].set_color('lightgray')
        ax[1].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=1.2)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_eccentricity_allcats_localsort.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_eccentricity_allcats_localsort.pdf',dpi=300)
        plt.show()

    elif not sep_cat_columns:

        sortorder_global = np.argsort(sortparam_sorted_locally)
        mpoNorm_sorted_globally = mpoNorm_sorted_locally[sortorder_global,:]
        mpoNorm_sorted_globally_ids = mpoNorm_sorted_locally_ids[sortorder_global,:]

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(9,6))

        ax[0].imshow(mpoNorm_sorted_globally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        cat_cmap = mcolors.ListedColormap(list(cat_colors.values()))
        ax[1].imshow(mpoNorm_sorted_globally_ids, cmap=cat_cmap, aspect=0.5)
        ax[1].invert_yaxis()
        ax[1].get_xaxis().set_visible(False)
        ax[1].get_yaxis().set_visible(False)
        ax[1].yaxis.tick_right()
        ax[1].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
        ax[1].margins(x=0, y=0)
        ax[1].spines['bottom'].set_color('lightgray')
        ax[1].spines['top'].set_color('lightgray')
        ax[1].spines['right'].set_color('lightgray')
        ax[1].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=1.2)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_eccentricity_allcats_globalsort.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_eccentricity_allcats_globalsort.pdf',dpi=300)
        plt.show()

    elif sep_cat_columns:

        sortorder_global = np.argsort(sortparam_sorted_locally)
        mpoNorm_sorted_globally = mpoNorm_sorted_locally[sortorder_global,:]
        mpoNorm_sorted_globally_ids = mpoNorm_sorted_locally_ids[sortorder_global,:]

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        wratios = np.append([20],[1 for i in catlist])
        fig,ax = plt.subplots(nrows=1,ncols=len(catlist)+1,sharey=False,sharex=False, figsize=(9,6), gridspec_kw={'width_ratios':wratios})

        ax[0].imshow(mpoNorm_sorted_globally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        for c in catlist:
            cat_cmap_new_list = np.append(['w'],[cat_colors[c]])
            cat_cmap = mcolors.ListedColormap(cat_cmap_new_list)
            mpoNorm_sorted_globally_ids_c = np.array([1 if x==c else 0 for x in mpoNorm_sorted_globally_ids])
            mpoNorm_sorted_globally_ids_c = np.expand_dims(mpoNorm_sorted_globally_ids_c, axis=1)
            ax[c].imshow(mpoNorm_sorted_globally_ids_c, cmap=cat_cmap, aspect='auto')
            ax[c].invert_yaxis()
            ax[c].get_xaxis().set_visible(False)
            ax[c].get_yaxis().set_visible(False)
            ax[c].yaxis.tick_right()
            ax[c].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
            ax[c].margins(x=0, y=0)
            # ax[c].axis('off')
            ax[c].spines['bottom'].set_color('lightgray')
            ax[c].spines['top'].set_color('lightgray')
            ax[c].spines['right'].set_color('lightgray')
            ax[c].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0.1, hspace=0, left=0, right=0.4)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_eccentricity_allcats_globalsort_sepcols.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_eccentricity_allcats_globalsort_sepcols.pdf',dpi=300)
        plt.show()

def plot_D_heatmap_ranked_locdensity_allcats(SAD, cellstats, cat_names=None, cat_colors=None, outf='Figures', boundthresh = 0.15, localsort=False, sep_cat_columns=True):
    '''
    For each cell category, generates a heatmap showing the distribution of inferred diffusion coefficients for each cell. Rank cells in the
    heatmap according to the mean number of localizations in each frame normalized by the area of the cell.
    Sorting can be performed within each category (localsort=True) or across all categories (localsort=False).
    If sorting is performed across categories, the category that the cell in each row of the heatmap belongs to can be visualized in either a
    single colorbar (sep_cat_columns=False) or in separate colorbars (sep_cat_columns=True).

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
        localsort: boolean indicating whether rows (cells) are to be sorted within each category (True) or globally (False)
        sep_cat_columns: boolean indicating whether the category that each cell belongs to is to be visualized in a single colorbar (False) or in separate colorbars (True)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    for c in range(len(catlist)):

        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
        D = SAD[catlist[c]].likelihood.diff_coefs
        sel = (D<=boundthresh)
        fbound = mpoNorm[:,sel].sum(1)
        numlocs = SAD[catlist[c]].processed_track_statistics['mean_detections_per_frame'].to_numpy()
        areas = cellstats[catlist[c]]['area_mask'].to_numpy()
        numlocs_per_area = numlocs/areas

        sortorder = np.argsort(numlocs_per_area)

        mpoNorm_sorted_locally_c = mpoNorm[sortorder,:]
        mpoNorm_sorted_locally_ids_c = np.zeros((mpoNorm.shape[0],1)) + catlist[c]
        if c == 0:
            mpoNorm_sorted_locally = mpoNorm_sorted_locally_c
            mpoNorm_sorted_locally_ids = mpoNorm_sorted_locally_ids_c
            sortparam_sorted_locally = numlocs_per_area[sortorder]
        else:
            mpoNorm_sorted_locally = np.append(mpoNorm_sorted_locally, mpoNorm_sorted_locally_c, axis=0)
            mpoNorm_sorted_locally_ids = np.append(mpoNorm_sorted_locally_ids, mpoNorm_sorted_locally_ids_c, axis=0)
            sortparam_sorted_locally = np.append(sortparam_sorted_locally, numlocs_per_area[sortorder])

    if localsort:

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(9,6))

        ax[0].imshow(mpoNorm_sorted_locally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        cat_cmap = mcolors.ListedColormap(list(cat_colors.values()))
        ax[1].imshow(mpoNorm_sorted_locally_ids, cmap=cat_cmap, aspect=0.5)
        ax[1].invert_yaxis()
        ax[1].get_xaxis().set_visible(False)
        ax[1].get_yaxis().set_visible(False)
        ax[1].yaxis.tick_right()
        ax[1].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
        ax[1].margins(x=0, y=0)
        ax[1].spines['bottom'].set_color('lightgray')
        ax[1].spines['top'].set_color('lightgray')
        ax[1].spines['right'].set_color('lightgray')
        ax[1].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=1.2)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_locdensity_allcats_localsort.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_locdensity_allcats_localsort.pdf',dpi=300)
        plt.show()

    elif not sep_cat_columns:

        sortorder_global = np.argsort(sortparam_sorted_locally)
        mpoNorm_sorted_globally = mpoNorm_sorted_locally[sortorder_global,:]
        mpoNorm_sorted_globally_ids = mpoNorm_sorted_locally_ids[sortorder_global,:]

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(9,6))

        ax[0].imshow(mpoNorm_sorted_globally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        cat_cmap = mcolors.ListedColormap(list(cat_colors.values()))
        ax[1].imshow(mpoNorm_sorted_globally_ids, cmap=cat_cmap, aspect=0.5)
        ax[1].invert_yaxis()
        ax[1].get_xaxis().set_visible(False)
        ax[1].get_yaxis().set_visible(False)
        ax[1].yaxis.tick_right()
        ax[1].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
        ax[1].margins(x=0, y=0)
        ax[1].spines['bottom'].set_color('lightgray')
        ax[1].spines['top'].set_color('lightgray')
        ax[1].spines['right'].set_color('lightgray')
        ax[1].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=1.2)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_locdensity_allcats_globalsort.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_locdensity_allcats_globalsort.pdf',dpi=300)
        plt.show()

    elif sep_cat_columns:

        sortorder_global = np.argsort(sortparam_sorted_locally)
        mpoNorm_sorted_globally = mpoNorm_sorted_locally[sortorder_global,:]
        mpoNorm_sorted_globally_ids = mpoNorm_sorted_locally_ids[sortorder_global,:]

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        wratios = np.append([20],[1 for i in catlist])
        fig,ax = plt.subplots(nrows=1,ncols=len(catlist)+1,sharey=False,sharex=False, figsize=(9,6), gridspec_kw={'width_ratios':wratios})

        ax[0].imshow(mpoNorm_sorted_globally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        for c in catlist:
            cat_cmap_new_list = np.append(['w'],[cat_colors[c]])
            cat_cmap = mcolors.ListedColormap(cat_cmap_new_list)
            mpoNorm_sorted_globally_ids_c = np.array([1 if x==c else 0 for x in mpoNorm_sorted_globally_ids])
            mpoNorm_sorted_globally_ids_c = np.expand_dims(mpoNorm_sorted_globally_ids_c, axis=1)
            ax[c].imshow(mpoNorm_sorted_globally_ids_c, cmap=cat_cmap, aspect='auto')
            ax[c].invert_yaxis()
            ax[c].get_xaxis().set_visible(False)
            ax[c].get_yaxis().set_visible(False)
            ax[c].yaxis.tick_right()
            ax[c].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
            ax[c].margins(x=0, y=0)
            # ax[c].axis('off')
            ax[c].spines['bottom'].set_color('lightgray')
            ax[c].spines['top'].set_color('lightgray')
            ax[c].spines['right'].set_color('lightgray')
            ax[c].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0.1, hspace=0, left=0, right=0.4)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_locdensity_allcats_globalsort_sepcols.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_locdensity_allcats_globalsort_sepcols.pdf',dpi=300)
        plt.show()

def plot_D_heatmap_ranked_cellcycle_ncratio_allcats(SAD, cellstats, cat_names=None, cat_colors=None, outf='Figures', boundthresh = 0.15, localsort=False, sep_cat_columns=True):
    '''
    For each cell category, generates a heatmap showing the distribution of inferred diffusion coefficients for each cell. Rank cells in the
    heatmap according to their DHB nuclear/cytoplasmic ratios.
    Sorting can be performed within each category (localsort=True) or across all categories (localsort=False).
    If sorting is performed across categories, the category that the cell in each row of the heatmap belongs to can be visualized in either a
    single colorbar (sep_cat_columns=False) or in separate colorbars (sep_cat_columns=True).

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
        localsort: boolean indicating whether rows (cells) are to be sorted within each category (True) or globally (False)
        sep_cat_columns: boolean indicating whether the category that each cell belongs to is to be visualized in a single colorbar (False) or in separate colorbars (True)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    if 'cellcycle_ncratio' in cellstats[catlist[0]].columns:
        pass
    else:
        print('No cell cycle marker channel found.')
        return

    for c in range(len(catlist)):

        mpo = SAD[catlist[c]].marginal_posterior_occs
        mpoNorm = mpo/mpo.sum(1)[:,np.newaxis] # get normalized PDF for each row
        D = SAD[catlist[c]].likelihood.diff_coefs
        sel = (D<=boundthresh)
        fbound = mpoNorm[:,sel].sum(1)
        ecc = cellstats[catlist[c]]['eccentricity'].to_numpy()
        cellcycle_ncratio = cellstats[catlist[c]]['cellcycle_ncratio'].to_numpy()
        not_on_boundary = list(cellstats[catlist[c]]['not_on_boundary'])

        # include only those that are not on the boundary
        # fbound = fbound[not_on_boundary]
        # ecc = ecc[not_on_boundary]
        # cellcycle_ncratio = cellcycle_ncratio[not_on_boundary]
        # mpoNorm = mpoNorm[not_on_boundary,:]

        sortorder = np.argsort(cellcycle_ncratio)

        mpoNorm_sorted_locally_c = mpoNorm[sortorder,:]
        mpoNorm_sorted_locally_ids_c = np.zeros((mpoNorm.shape[0],1)) + catlist[c]
        if c == 0:
            mpoNorm_sorted_locally = mpoNorm_sorted_locally_c
            mpoNorm_sorted_locally_ids = mpoNorm_sorted_locally_ids_c
            sortparam_sorted_locally = cellcycle_ncratio[sortorder]
        else:
            mpoNorm_sorted_locally = np.append(mpoNorm_sorted_locally, mpoNorm_sorted_locally_c, axis=0)
            mpoNorm_sorted_locally_ids = np.append(mpoNorm_sorted_locally_ids, mpoNorm_sorted_locally_ids_c, axis=0)
            sortparam_sorted_locally = np.append(sortparam_sorted_locally, cellcycle_ncratio[sortorder])

    if localsort:

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(9,6))

        ax[0].imshow(mpoNorm_sorted_locally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        cat_cmap = mcolors.ListedColormap(list(cat_colors.values()))
        ax[1].imshow(mpoNorm_sorted_locally_ids, cmap=cat_cmap, aspect=0.5)
        ax[1].invert_yaxis()
        ax[1].get_xaxis().set_visible(False)
        ax[1].get_yaxis().set_visible(False)
        ax[1].yaxis.tick_right()
        ax[1].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
        ax[1].margins(x=0, y=0)
        ax[1].spines['bottom'].set_color('lightgray')
        ax[1].spines['top'].set_color('lightgray')
        ax[1].spines['right'].set_color('lightgray')
        ax[1].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=1.2)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_cellcycle_ncratio_allcats_localsort.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_cellcycle_ncratio_allcats_localsort.pdf',dpi=300)
        plt.show()

    elif not sep_cat_columns:

        sortorder_global = np.argsort(sortparam_sorted_locally)
        mpoNorm_sorted_globally = mpoNorm_sorted_locally[sortorder_global,:]
        mpoNorm_sorted_globally_ids = mpoNorm_sorted_locally_ids[sortorder_global,:]

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        fig,ax = plt.subplots(nrows=1,ncols=2,sharey=False,sharex=False, figsize=(9,6))

        ax[0].imshow(mpoNorm_sorted_globally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        cat_cmap = mcolors.ListedColormap(list(cat_colors.values()))
        ax[1].imshow(mpoNorm_sorted_globally_ids, cmap=cat_cmap, aspect=0.5)
        ax[1].invert_yaxis()
        ax[1].get_xaxis().set_visible(False)
        ax[1].get_yaxis().set_visible(False)
        ax[1].yaxis.tick_right()
        ax[1].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
        ax[1].margins(x=0, y=0)
        ax[1].spines['bottom'].set_color('lightgray')
        ax[1].spines['top'].set_color('lightgray')
        ax[1].spines['right'].set_color('lightgray')
        ax[1].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0, hspace=0, left=0, right=1.2)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_cellcycle_ncratio_allcats_globalsort.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_cellcycle_ncratio_allcats_globalsort.pdf',dpi=300)
        plt.show()

    elif sep_cat_columns:

        sortorder_global = np.argsort(sortparam_sorted_locally)
        mpoNorm_sorted_globally = mpoNorm_sorted_locally[sortorder_global,:]
        mpoNorm_sorted_globally_ids = mpoNorm_sorted_locally_ids[sortorder_global,:]

        xticks = np.array([0,24,49,74,99])
        xticklabel = ['0.01','0.1','1','10','100']

        wratios = np.append([20],[1 for i in catlist])
        fig,ax = plt.subplots(nrows=1,ncols=len(catlist)+1,sharey=False,sharex=False, figsize=(9,6), gridspec_kw={'width_ratios':wratios})

        ax[0].imshow(mpoNorm_sorted_globally,vmin=0,vmax=0.04,aspect='auto')
        ax[0].set_ylabel('Cell number')
        ax[0].set_xlabel('$D$ (µm$^2$/s)')
        ax[0].set_xticks(xticks)
        ax[0].set_xticklabels(xticklabel)
        ax[0].invert_yaxis()
        ax[0].margins(x=0, y=0)

        for c in catlist:
            cat_cmap_new_list = np.append(['w'],[cat_colors[c]])
            cat_cmap = mcolors.ListedColormap(cat_cmap_new_list)
            mpoNorm_sorted_globally_ids_c = np.array([1 if x==c else 0 for x in mpoNorm_sorted_globally_ids])
            mpoNorm_sorted_globally_ids_c = np.expand_dims(mpoNorm_sorted_globally_ids_c, axis=1)
            ax[c].imshow(mpoNorm_sorted_globally_ids_c, cmap=cat_cmap, aspect='auto')
            ax[c].invert_yaxis()
            ax[c].get_xaxis().set_visible(False)
            ax[c].get_yaxis().set_visible(False)
            ax[c].yaxis.tick_right()
            ax[c].set_yticks(np.cumsum([SAD[i].n_files for i in catlist]))
            ax[c].margins(x=0, y=0)
            # ax[c].axis('off')
            ax[c].spines['bottom'].set_color('lightgray')
            ax[c].spines['top'].set_color('lightgray')
            ax[c].spines['right'].set_color('lightgray')
            ax[c].spines['left'].set_color('lightgray')

        fig.subplots_adjust(wspace=0.1, hspace=0, left=0, right=0.4)
        fig.tight_layout()

        fig.savefig(outf+'/heatmap_ranked_cellcycle_ncratio_allcats_globalsort_sepcols.png',dpi=300)
        fig.savefig(outf+'/heatmap_ranked_cellcycle_ncratio_allcats_globalsort_sepcols.pdf',dpi=300)
        plt.show()

def plot_intensity_pairplots(SAD, cellstats, alt_snapsfolders_names, default_channel_name='Main channel', cat_names=None, cat_colors=None, outf='Figures'):
    '''
    Generates pairplot showing the distribution of intensities in each channel, with experiments and cell categories
    identified by different colors.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        alt_snapsfolders_names: list of names of alternative channels to be analyzed
        default_channel_name: name of the channel used to generate cell masks
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    # concatenate all cellstats dataframes (for different cell categories) into one
    for c in range(len(catlist)):
        cellstats[catlist[c]]['cat_id'] = catlist[c]
    cdata = pd.concat([cellstats[catlist[c]] for c in range(len(catlist))])

    vars = ['intensity_mask_'+alt_snapsfolders_name for alt_snapsfolders_name in alt_snapsfolders_names]
    vars = ['experiment_idx', 'cat_id', 'intensity_mask', 'cluster', 'cluster_allcats'] + vars
    cdata_plot = cdata[vars]
    col_rename_map = {"intensity_mask": f"Mean {default_channel_name} intensity (AU)"}
    for alt_snapsfolders_name in alt_snapsfolders_names:
        exec('col_rename_map["intensity_mask_'+alt_snapsfolders_name+'"] = "Mean '+alt_snapsfolders_name+' intensity (AU)"')
    cdata_plot = cdata_plot.rename(col_rename_map, axis=1)
    vars_new = [f'Mean {default_channel_name} intensity (AU)'] + ['Mean '+alt_snapsfolders_name + ' intensity (AU)' for alt_snapsfolders_name in alt_snapsfolders_names]
    cdata_plot.reset_index(level=None, drop=True, inplace=True)


    g = sns.PairGrid(cdata_plot, vars=vars_new, hue='experiment_idx', palette='Paired', diag_sharey=False)
    g.map_diag(sns.histplot, element="step", stat='count')
    g.map_offdiag(sns.scatterplot)
    g.add_legend(title='Experiment')
    plt.savefig(outf+'/intensities_pairplot_expcolor.png',dpi=300)
    plt.savefig(outf+'/intensities_pairplot_expcolor.pdf',dpi=300)
    plt.show()

    g = sns.PairGrid(cdata_plot, vars=vars_new, hue='cat_id', palette=cat_colors, diag_sharey=False)
    g.map_diag(sns.histplot, element="step", stat='count')
    g.map_offdiag(sns.scatterplot)
    g.add_legend(title='Cell category')
    plt.savefig(outf+'/intensities_pairplot_catcolor.png',dpi=300)
    plt.savefig(outf+'/intensities_pairplot_catcolor.pdf',dpi=300)
    plt.show()

    cluster_indices = list(cdata_plot['cluster_allcats'].values)
    cluster_sizes = [[x,cluster_indices.count(x)] for x in set(cluster_indices)]
    cluster_sortorder_indices = np.argsort([x[1] for x in cluster_sizes])
    cluster_sortorder = [cluster_sizes[x][0] for x in cluster_sortorder_indices]
    cluster_sortorder.reverse()
    cdata_plot_new_clusters = pd.concat([cdata_plot.loc[(cdata_plot['cluster_allcats']==cluster_sortorder[c])] for c in range(len(cluster_sortorder))])
    g = sns.PairGrid(cdata_plot_new_clusters, vars=vars_new, hue='cluster_allcats', palette="tab10", diag_sharey=False)
    g.map_diag(sns.histplot, element="step", stat='count')
    g.map_offdiag(sns.scatterplot)
    g.add_legend(title='Cluster (across categories)')
    plt.savefig(outf+'/intensities_pairplot_clustercolor_allcats.png',dpi=300)
    plt.savefig(outf+'/intensities_pairplot_clustercolor.pdf',dpi=300)
    plt.show()

    for c in range(len(catlist)):	
        category_ID = catlist[c]	
        cdata_plot_c = cdata_plot.loc[(cdata_plot['cat_id']==category_ID)]	
        cluster_indices = list(cdata_plot_c['cluster'].values)
        cluster_sizes = [[x,cluster_indices.count(x)] for x in set(cluster_indices)]
        cluster_sortorder_indices = np.argsort([x[1] for x in cluster_sizes])
        cluster_sortorder = [cluster_sizes[x][0] for x in cluster_sortorder_indices]
        cluster_sortorder.reverse()
        cdata_plot_new_clusters = pd.concat([cdata_plot_c.loc[(cdata_plot_c['cluster']==cluster_sortorder[c])] for c in range(len(cluster_sortorder))])
        g = sns.PairGrid(cdata_plot_new_clusters, vars=vars_new, hue='cluster', palette='tab10', diag_sharey=False)	
        g.map_diag(sns.histplot, element="step", stat='count')	
        g.map_offdiag(sns.scatterplot)	
        g.add_legend(title='Cluster in '+cat_names[category_ID]+' cells')	
        plt.savefig(outf+'/intensities_pairplot_clustercolor_'+cat_names[category_ID]+'.png',dpi=300)
        plt.savefig(outf+'/intensities_pairplot_clustercolor_'+cat_names[category_ID]+'.pdf',dpi=300)	
        plt.show()

def plot_channel_pairplots_otherparams(SAD, cellstats, alt_snapsfolders_names, default_channel_name='Main channel', cat_names=None, cat_colors=None, outf='Figures'):
    '''
    Generates pairplots of scatter plots between intensities in different channels and various other
    morphological/tracking/diffusivity features.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        alt_snapsfolders_names: list of names of alternative channels to be analyzed
        default_channel_name: name of the channel used to generate cell masks
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    # concatenate all cellstats dataframes (for different cell categories) into one
    for c in range(len(catlist)):
        cellstats[catlist[c]]['cat_id'] = catlist[c]
    cdata = pd.concat([cellstats[catlist[c]] for c in range(len(catlist))], ignore_index=True)
    areas_realunits = np.zeros((len(cdata),1))
    for i in range(len(cdata)):
        areas_realunits[i] = cdata.iloc[i]['area_mask']*(SAD[cdata.iloc[i]['cat_id']].likelihood.pixel_size_um)**2
    cdata['area_realunits'] = areas_realunits

    vars = ['intensity_mask_'+alt_snapsfolders_name for alt_snapsfolders_name in alt_snapsfolders_names]
    if 'cellcycle_ncratio' in cellstats[catlist[0]].columns:
        y_vars = ['area_realunits', 'eccentricity', 'fbound', 'D_peak', 'cellcycle_ncratio']
    else:
        y_vars = ['area_realunits', 'eccentricity', 'fbound', 'D_peak']
    vars = ['experiment_idx', 'cat_id', 'intensity_mask', 'not_on_boundary', 'cluster', 'cluster_allcats'] + vars + y_vars
    cdata_plot = cdata[vars]
    if 'cellcycle_ncratio' in cellstats[catlist[0]].columns:
        col_rename_map = {"intensity_mask": f"Mean {default_channel_name} intensity (AU)", "area_realunits": "Area (µm\u00b2)", "eccentricity":"Eccentricity", "fbound": "Fraction bound", "D_peak": "Modal diffusivity (µm\u00b2/s)", "cellcycle_ncratio": "DHB N/C ratio"}
    else:
        col_rename_map = {"intensity_mask": f"Mean {default_channel_name} intensity (AU)", "area_realunits": "Area (µm\u00b2)", "eccentricity":"Eccentricity", "fbound": "Fraction bound", "D_peak": "Modal diffusivity (µm\u00b2/s)"}
    for alt_snapsfolders_name in alt_snapsfolders_names:
        exec('col_rename_map["intensity_mask_'+alt_snapsfolders_name+'"] = "Mean '+alt_snapsfolders_name+' intensity (AU)"')
    cdata_plot = cdata_plot.rename(col_rename_map, axis=1)
    cdata_plot = cdata_plot.loc[(cdata_plot['not_on_boundary']==True)]
    vars_new = [f'Mean {default_channel_name} intensity (AU)'] + ['Mean '+alt_snapsfolders_name+' intensity (AU)' for alt_snapsfolders_name in alt_snapsfolders_names]
    if 'cellcycle_ncratio' in cellstats[catlist[0]].columns:
        y_vars_new = ['Area (µm\u00b2)', 'Eccentricity', 'Fraction bound', 'Modal diffusivity (µm\u00b2/s)', 'DHB N/C ratio']
    else:
        y_vars_new = ['Area (µm\u00b2)', 'Eccentricity', 'Fraction bound', 'Modal diffusivity (µm\u00b2/s)']

    g = sns.PairGrid(cdata_plot, x_vars=vars_new, y_vars=y_vars_new, hue='experiment_idx', palette='Paired')
    g.map(sns.scatterplot)
    g.add_legend(title='Experiment')
    plt.savefig(outf+'/intensity_otherparams_pairplot_expcolor.png',dpi=300)
    plt.savefig(outf+'/intensity_otherparams_pairplot_expcolor.pdf',dpi=300)
    plt.show()

    g = sns.PairGrid(cdata_plot, x_vars=vars_new, y_vars=y_vars_new, hue='cat_id', palette=cat_colors)
    g.map(sns.scatterplot)
    g.add_legend(title='Cell category')
    plt.savefig(outf+'/intensity_otherparams_pairplot_catcolor.png',dpi=300)
    plt.savefig(outf+'/intensity_otherparams_pairplot_catcolor.pdf',dpi=300)
    plt.show()

    cluster_indices = list(cdata_plot['cluster_allcats'].values)
    cluster_sizes = [[x,cluster_indices.count(x)] for x in set(cluster_indices)]
    cluster_sortorder_indices = np.argsort([x[1] for x in cluster_sizes])
    cluster_sortorder = [cluster_sizes[x][0] for x in cluster_sortorder_indices]
    cluster_sortorder.reverse()
    cdata_plot_new_clusters = pd.concat([cdata_plot.loc[(cdata_plot['cluster_allcats']==cluster_sortorder[c])] for c in range(len(cluster_sortorder))])
    g = sns.PairGrid(cdata_plot_new_clusters, x_vars=vars_new, y_vars=y_vars_new, hue='cluster_allcats', palette="tab10")
    g.map(sns.scatterplot)
    g.add_legend(title='Cluster (across categories)')
    plt.savefig(outf+'/intensity_otherparams_pairplot_clustercolor_allcats.png',dpi=300)
    plt.savefig(outf+'/intensity_otherparams_pairplot_clustercolor_allcats.pdf',dpi=300)
    plt.show()

    for c in range(len(catlist)):	
        category_ID = catlist[c]	
        cdata_plot_c = cdata_plot.loc[(cdata_plot['cat_id']==category_ID)]	
        cluster_indices = list(cdata_plot_c['cluster'].values)
        cluster_sizes = [[x,cluster_indices.count(x)] for x in set(cluster_indices)]
        cluster_sortorder_indices = np.argsort([x[1] for x in cluster_sizes])
        cluster_sortorder = [cluster_sizes[x][0] for x in cluster_sortorder_indices]
        cluster_sortorder.reverse()
        cdata_plot_new_clusters = pd.concat([cdata_plot_c.loc[(cdata_plot_c['cluster']==cluster_sortorder[c])] for c in range(len(cluster_sortorder))])
        g = sns.PairGrid(cdata_plot_new_clusters, x_vars=vars_new, y_vars=y_vars_new, hue='cluster', palette='tab10')	
        g.map(sns.scatterplot)	
        g.add_legend(title='Cluster in '+cat_names[category_ID]+' cells')	
        plt.savefig(outf+'/intensity_otherparams_pairplot_clustercolor_'+cat_names[category_ID]+'.png',dpi=300)
        plt.savefig(outf+'/intensity_otherparams_pairplot_clustercolor_'+cat_names[category_ID]+'.pdf',dpi=300)	
        plt.show()

def plot_otherparams_pairplots(SAD, cellstats, alt_snapsfolders_names, default_channel_name='Main channel', cat_names=None, cat_colors=None, outf='Figures'):
    '''
    Generates pairplots of scatter plots between various morphological/tracking/diffusivity features.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        alt_snapsfolders_names: list of names of alternative channels to be analyzed
        default_channel_name: name of the channel used to generate cell masks
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    # concatenate all cellstats dataframes (for different cell categories) into one
    for c in range(len(catlist)):
        cellstats[catlist[c]]['cat_id'] = catlist[c]
    cdata = pd.concat([cellstats[catlist[c]] for c in range(len(catlist))])
    areas_realunits = np.zeros((len(cdata),1))
    for i in range(len(cdata)):
        areas_realunits[i] = cdata.iloc[i]['area_mask']*(SAD[cdata.iloc[i]['cat_id']].likelihood.pixel_size_um)**2
    cdata['area_realunits'] = areas_realunits

    vars = ['intensity_mask_'+alt_snapsfolders_name for alt_snapsfolders_name in alt_snapsfolders_names]
    if 'cellcycle_ncratio' in cellstats[catlist[0]].columns:
        y_vars = ['area_realunits', 'eccentricity', 'fbound', 'D_peak', 'cellcycle_ncratio']
    else:
        y_vars = ['area_realunits', 'eccentricity', 'fbound', 'D_peak']
    vars = ['experiment_idx', 'cat_id', 'intensity_mask', 'not_on_boundary', 'cluster', 'cluster_allcats'] + vars + y_vars
    cdata_plot = cdata[vars]
    if 'cellcycle_ncratio' in cellstats[catlist[0]].columns:
        col_rename_map = {"intensity_mask": f"Mean {default_channel_name} intensity (AU)", "area_realunits": "Area (µm\u00b2)", "eccentricity":"Eccentricity", "fbound": "Fraction bound", "D_peak": "Modal diffusivity (µm\u00b2/s)", "cellcycle_ncratio": "DHB N/C ratio"}
    else:
        col_rename_map = {"intensity_mask": f"Mean {default_channel_name} intensity (AU)", "area_realunits": "Area (µm\u00b2)", "eccentricity":"Eccentricity", "fbound": "Fraction bound", "D_peak": "Modal diffusivity (µm\u00b2/s)"}
    for alt_snapsfolders_name in alt_snapsfolders_names:
        exec('col_rename_map["intensity_mask_'+alt_snapsfolders_name+'"] = "Mean '+alt_snapsfolders_name+' intensity (AU)"')
    cdata_plot = cdata_plot.rename(col_rename_map, axis=1)
    cdata_plot = cdata_plot.loc[(cdata_plot['not_on_boundary']==True)]
    vars_new = [f'Mean {default_channel_name} intensity (AU)'] + ['Mean '+alt_snapsfolders_name+' intensity (AU)' for alt_snapsfolders_name in alt_snapsfolders_names]
    if 'cellcycle_ncratio' in cellstats[catlist[0]].columns:
        y_vars_new = ['Area (µm\u00b2)', 'Eccentricity', 'Fraction bound', 'Modal diffusivity (µm\u00b2/s)', 'DHB N/C ratio']
    else:
        y_vars_new = ['Area (µm\u00b2)', 'Eccentricity', 'Fraction bound', 'Modal diffusivity (µm\u00b2/s)']

    g = sns.PairGrid(cdata_plot, x_vars=y_vars_new, y_vars=y_vars_new, hue='experiment_idx', palette='Paired', diag_sharey=False)
    g.map_diag(sns.histplot, element="step", stat='count')
    g.map_offdiag(sns.scatterplot)
    g.add_legend(title='Experiment')
    plt.savefig(outf+'/otherparams_pairplot_expcolor.png',dpi=300)
    plt.savefig(outf+'/otherparams_pairplot_expcolor.pdf',dpi=300)
    plt.show()

    g = sns.PairGrid(cdata_plot, x_vars=y_vars_new, y_vars=y_vars_new, hue='cat_id', palette=cat_colors, diag_sharey=False)
    g.map_diag(sns.histplot, element="step", stat='count')
    g.map_offdiag(sns.scatterplot)
    g.add_legend(title='Cell category')
    plt.savefig(outf+'/otherparams_pairplot_catcolor.png',dpi=300)
    plt.savefig(outf+'/otherparams_pairplot_catcolor.pdf',dpi=300)
    plt.show()

    cluster_indices = list(cdata_plot['cluster_allcats'].values)
    cluster_sizes = [[x,cluster_indices.count(x)] for x in set(cluster_indices)]
    cluster_sortorder_indices = np.argsort([x[1] for x in cluster_sizes])
    cluster_sortorder = [cluster_sizes[x][0] for x in cluster_sortorder_indices]
    cluster_sortorder.reverse()
    cdata_plot_new_clusters = pd.concat([cdata_plot.loc[(cdata_plot['cluster_allcats']==cluster_sortorder[c])] for c in range(len(cluster_sortorder))])
    g = sns.PairGrid(cdata_plot_new_clusters, x_vars=y_vars_new, y_vars=y_vars_new, hue='cluster_allcats', palette="tab10", diag_sharey=False)
    g.map_diag(sns.histplot, element="step", stat='count')
    g.map_offdiag(sns.scatterplot)
    g.add_legend(title='Cluster (across categories)')
    plt.savefig(outf+'/otherparams_pairplot_clustercolor_allcats.png',dpi=300)
    plt.savefig(outf+'/otherparams_pairplot_clustercolor_allcats.pdf',dpi=300)
    plt.show()

    for c in range(len(catlist)):	
        category_ID = catlist[c]	
        cdata_plot_c = cdata_plot.loc[(cdata_plot['cat_id']==category_ID)]	
        cluster_indices = list(cdata_plot_c['cluster'].values)
        cluster_sizes = [[x,cluster_indices.count(x)] for x in set(cluster_indices)]
        cluster_sortorder_indices = np.argsort([x[1] for x in cluster_sizes])
        cluster_sortorder = [cluster_sizes[x][0] for x in cluster_sortorder_indices]
        cluster_sortorder.reverse()
        cdata_plot_new_clusters = pd.concat([cdata_plot_c.loc[(cdata_plot_c['cluster']==cluster_sortorder[c])] for c in range(len(cluster_sortorder))])
        g = sns.PairGrid(cdata_plot_new_clusters, x_vars=y_vars_new, y_vars=y_vars_new, hue='cluster', palette='tab10', diag_sharey=False)	
        g.map_diag(sns.histplot, element="step", stat='count')
        g.map_offdiag(sns.scatterplot)	
        g.add_legend(title='Cluster in '+cat_names[category_ID]+' cells')	
        plt.savefig(outf+'/otherparams_pairplot_clustercolor_'+cat_names[category_ID]+'.png',dpi=300)
        plt.savefig(outf+'/otherparams_pairplot_clustercolor_'+cat_names[category_ID]+'.pdf',dpi=300)	
        plt.show()

def cluster_spectra(SAD, cellstats, distance_metric='euclidean', cat_names=None, cat_colors=None, outf='Figures', boundthresh = 0.15):

    '''
    Performs hierarchical clustering on the diffusion spectra of a given cell category. Plots the diffusion spectra colored according to their
    assigned clusters and adds the cluster index to the cellstats dataframe.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        distance_metric: distance metric to be used for hierarchical clustering
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    for c in range(len(catlist)):	
        category_ID = catlist[c]	
        D = SAD[category_ID].likelihood.diff_coefs
        sel = (D<=boundthresh)
        mpo = SAD[category_ID].marginal_posterior_occs	
        mpoNorm = mpo/mpo.sum(axis=1)[:,np.newaxis]	
        
        # compute a distance matrix based on the user-specified distance metric	
        dist_matrix = scipy.spatial.distance.squareform(scipy.spatial.distance.pdist(mpoNorm, metric=distance_metric))	
        # set a threshold distance for the hierarchical clustering algorithm	
        d_threshold = np.mean(dist_matrix.flatten()) + 2*np.std(dist_matrix.flatten())	
        # perform hierarchical clustering	
        clustering = sklearn.cluster.AgglomerativeClustering(n_clusters=None, affinity="precomputed", linkage="complete", distance_threshold=d_threshold).fit(dist_matrix)	
        # update cellstats with clustering results	
        cellstats[category_ID]['cluster'] = clustering.labels_	
        
        # plot diffusion spectra colored by clusters	
        cluster_colors = sns.color_palette("tab10", len(np.unique(clustering.labels_)))	
        for cluster in np.unique(clustering.labels_):	
            plt.semilogx(D, mpoNorm[clustering.labels_==cluster,:].T[:,0], color = cluster_colors[cluster], label='Cluster ' + str(cluster))
            if 	mpoNorm[clustering.labels_==cluster,:].T.shape[1] > 1:
                plt.semilogx(D, mpoNorm[clustering.labels_==cluster,:].T[:,1:], color = cluster_colors[cluster])	
        plt.legend()
        plt.title(cat_names[catlist[c]])
        plt.xlim([0.01,100])
        plt.gca().set_xticks([0.01,0.1,1,10,100])
        plt.gca().set_xticklabels(["0.01", "0.1", "1", "10", "100"])
        plt.xlabel('$D$ (µm$^2$/s)')
        plt.ylabel('Fraction of molecules')
        plt.savefig(outf+'/clustered_spectra_'+cat_names[category_ID]+'.png',dpi=300)
        plt.savefig(outf+'/clustered_spectra_'+cat_names[category_ID]+'.pdf',dpi=300)	
        plt.show()

        for cluster in np.unique(clustering.labels_):
            mpo_cluster = mpo[clustering.labels_==cluster,:]
            mpoNorm_cluster = np.sum(mpo_cluster, axis=0)/np.sum(mpo_cluster)
            print("Cluster "+str(cluster)+":")
            print("Number of cells = "+str(mpo_cluster.shape[0]))
            print("fbound = "+str(mpoNorm_cluster[sel].sum()))

    return cellstats

def cluster_spectra_allcats(SAD, cellstats, distance_metric='euclidean', cat_names=None, cat_colors=None, outf='Figures', boundthresh = 0.15):

    '''
    Performs hierarchical clustering on the diffusion spectra of all cells across categories. Plots the diffusion spectra colored according to their
    assigned clusters and adds the cluster index to the cellstats dataframe. Computes the enrichment of different categories in each cluster.

    Inputs:
        SAD: dictionary of SASPT state array datasets for each cell category
        cellstats: dictionary generated by the package_cellstats function
        distance_metric: distance metric to be used for hierarchical clustering
        cat_names: dictionary of category names, with category IDs (from CellPicker) as keys
        cat_colors: dictionary of colors to be used to plot category-specific data, with category IDs (from CellPicker) as keys
        outf: path to figure output directory
        boundthresh: diffusivity cutoff below which a molecule is considered bound (in µm-squared per second)
    '''

    # make output directory
    os.makedirs(outf, exist_ok = True)

    catlist = list(SAD.keys())
    if (cat_names is None) or (len(cat_names)!=len(catlist)):
        catstrings = ['Category '+str(x) for x in catlist]
        cat_names  = dict((key,value) for key,value in zip(catlist,catstrings))
    if (cat_colors is None) or (len(cat_colors)!=len(catlist)):
        colornames = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
        cat_colors = dict((key,value) for key,value in zip(catlist,colornames))

    D = SAD[catlist[0]].likelihood.diff_coefs
    sel = (D<=boundthresh)
    mpo_allcats = [SAD[category_ID].marginal_posterior_occs for category_ID in catlist]
    mpo_catIDs = [np.zeros((mpo_allcats[c].shape[0],1)) + catlist[c] for c in range(len(catlist))]
    mpo_allcats = np.concatenate(mpo_allcats, axis=0)
    mpoNorm_allcats = mpo_allcats/mpo_allcats.sum(axis=1)[:,np.newaxis]
    mpo_catIDs = np.concatenate(mpo_catIDs, axis=0)

    # compute a distance matrix based on the user-specified distance metric	
    dist_matrix = scipy.spatial.distance.squareform(scipy.spatial.distance.pdist(mpoNorm_allcats, metric=distance_metric))	
    # set a threshold distance for the hierarchical clustering algorithm	
    d_threshold = np.mean(dist_matrix.flatten()) + 2*np.std(dist_matrix.flatten())	
    # perform hierarchical clustering	
    clustering = sklearn.cluster.AgglomerativeClustering(n_clusters=None, affinity="precomputed", linkage="complete", distance_threshold=d_threshold).fit(dist_matrix)	
    # update cellstats with clustering results	
    for c in range(len(catlist)):
        cellstats[catlist[c]]['cluster_allcats'] = clustering.labels_[mpo_catIDs[:,0]==catlist[c]]

    # plot diffusion spectra colored by clusters	
    cluster_colors = sns.color_palette("tab10", len(np.unique(clustering.labels_)))
    for c in range(len(catlist)):
        clusteringlabels_c = clustering.labels_[mpo_catIDs[:,0]==catlist[c]]
        mpoNorm_c = mpoNorm_allcats[mpo_catIDs[:,0]==catlist[c],:]
        for cluster in np.unique(clusteringlabels_c):	
            plt.semilogx(D, mpoNorm_c[clusteringlabels_c==cluster,:].T[:,0], color = cluster_colors[cluster], label='Cluster ' + str(cluster))
            if 	mpoNorm_c[clusteringlabels_c==cluster,:].T.shape[1] > 1:
                plt.semilogx(D, mpoNorm_c[clusteringlabels_c==cluster,:].T[:,1:], color = cluster_colors[cluster])	
        plt.title(cat_names[catlist[c]])
        plt.xlim([0.01,100])
        plt.gca().set_xticks([0.01,0.1,1,10,100])
        plt.gca().set_xticklabels(["0.01", "0.1", "1", "10", "100"])
        plt.xlabel('$D$ (µm$^2$/s)')
        plt.ylabel('Fraction of molecules')
        plt.legend()	
        plt.savefig(outf+'/clustering_across_cats_'+cat_names[catlist[c]]+'.png',dpi=300)
        plt.savefig(outf+'/clustering_across_cats_'+cat_names[catlist[c]]+'.pdf',dpi=300)
        plt.show()

    # compute enrichment of each category in each cluster
    print("Enrichment of cell categories in each cluster:")
    for cluster in np.unique(clustering.labels_):
        mpo_allcats_cluster = mpo_allcats[clustering.labels_==cluster,:]
        mpoNorm_allcats_cluster = np.sum(mpo_allcats_cluster, axis=0)/np.sum(mpo_allcats_cluster)
        mpo_catIDs_cluster = mpo_catIDs[:,0][clustering.labels_==cluster]
        D_peak_cluster = D[np.argmax(mpoNorm_allcats_cluster, axis=0)]
        print("Cluster "+str(cluster)+":")
        print("Number of cells = "+str(len(mpo_catIDs_cluster)))
        print("fbound = "+str(mpoNorm_allcats_cluster[sel].sum()))
        print("Modal diffusion coefficient = "+str(D_peak_cluster)+" µm\u00b2/s")
        for c in range(len(catlist)):
            print("Enrichment of "+cat_names[catlist[c]]+" in cluster "+str(cluster)+" = "+str(np.sum(mpo_catIDs_cluster==catlist[c])/len(mpo_catIDs_cluster)*100)+" %")
        print()

    # compute fraction of each category that falls into each cluster
    print()
    print("Fraction of each cell category in each cluster:")
    for c in range(len(catlist)):
        print(cat_names[catlist[c]]+":")
        cellstats_c = cellstats[catlist[c]]
        for cluster in np.unique(clustering.labels_):
            print("Fraction of "+cat_names[catlist[c]]+" cells in cluster "+str(cluster)+" = "+str(len(cellstats_c[cellstats_c['cluster_allcats']==cluster])/len(cellstats_c)*100)+" %")

    return cellstats

# --------------------------------------------------------------------------------------------------
# DHB-GFP marker-based cell cycle analysis functions by Vinson Fan

mask_folder = '/Volumes/VF_DATA1/Nike_CellCycle/20230621_U2OS_C32_20nM_DHB-mNG_d2/masks'
#rois = '/Volumes/VF_DATA1/Nike_CellCycle/20230621_U2OS_C32_20nM_DHB-mNG_d2/rois.txt'
image_folder = '/Volumes/VF_DATA1/Nike_CellCycle/20230621_U2OS_C32_20nM_DHB-mNG_d2/snaps8'


def multiple_dilation(mask: np.ndarray, n: int) -> np.ndarray:
    """Dilate a mask n times."""
    for i in range(n):
        mask = binary_dilation(mask)
    return mask


def calculate_nuclear_ring_intensities(mask: np.ndarray,
                                       image: np.ndarray,
                                       background_subtract: str='rolling_ball',
                                       inner_rad: int=10,
                                       outer_rad: int=20,
                                       show_plot: bool=False,
                                       out_png: str=None) -> dict:
    """Calculate nuclear intensity and the intensity of 
    a ring around the nucleus ring intensities."""

    assert mask.shape == image.shape, "Mask and image must be the same shape!"

    stats = {'avg_nuclear_intensity': [],
             'avg_ring_intensity': [],
             'nuclear_ring_ratio': [],
             'nuclear_area': [],
             'mask_index': []}
    ring_masks = np.zeros(mask.shape, dtype=int)

    # Iterate through masks in one image
    for i in np.unique(mask):
        # Skip background
        if i == 0:
            continue
        
        # Get nuclear mask and calculate ring mask
        nuclear_mask = mask == i
        nuclear_area = np.sum(nuclear_mask)
        if nuclear_area < 300:
            continue
        outer_ring = multiple_dilation(nuclear_mask, outer_rad)
        inner_ring = multiple_dilation(nuclear_mask, inner_rad)  
        ring = outer_ring ^ inner_ring
        ring_masks[ring] = i

        # Get nuclear and ring intensities
        if background_subtract == 'rolling_ball':
            pass
            #max_dim = np.maximum(image.shape[0], image.shape[1])
            #bg = rolling_ball(image, radius=max_dim//2)
            #image -= bg
        nuclear_intensity = np.average(image[nuclear_mask])
        ring_vals = image[ring]
        ring_intensity = ring_vals[np.where(ring_vals > np.percentile(ring_vals, 50))[0]]
        ring_intensity = np.average(ring_intensity)
        nuc_ring_ratio = nuclear_intensity / ring_intensity

        # Store stats
        stats['avg_nuclear_intensity'].append(nuclear_intensity)
        stats['avg_ring_intensity'].append(ring_intensity)
        stats['nuclear_ring_ratio'].append(nuc_ring_ratio)
        stats['nuclear_area'].append(nuclear_area)
        stats['mask_index'].append(i)
    
    if show_plot or out_png is not None:
        _, axs = plt.subplots(1, 3, figsize=(12, 4))
        axs[0].imshow(image, cmap='gray')
        axs[1].imshow(mask)
        axs[2].imshow(ring_masks)
        axs[0].set_title('GFP image')
        axs[1].set_title('Nuclear masks')
        axs[2].set_title('Ring masks')
        if out_png is not None:
            # modified by Sathvik Anantakrishnan to change dpi and save pdfs
            plt.savefig(out_png+'.png', dpi=300)
            plt.savefig(out_png+'.pdf', dpi=300)
        if show_plot:
            plt.show()
        plt.close()
    
    return stats

# --------------------------------------------------------------------------------------------------